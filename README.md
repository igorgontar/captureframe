# Skype Presenter package

There are two utilities in the package:
* **Edge Presenter** - makes possible to present Windows Edge Browser
* **Frame Presenter** - provides more fine grained control over shared content  

## Edge Presenter

This utility is designed to solve the problem with sharing of Microsoft Edge Browser over Skype, which is a big joke on the market:
> "Standard Windows tool for presenting applications can't not present standard Windows Browser..."

#### How to use

1. Build or download and unzip  ```EdgePresenter.exe``` from the release section. 
Releases can be accessed by hovering mouse over *Project overview* menu on the 
left side of the project's main web page. You should see it, if you are reading this. 
2. Launch ```EdgePresenter.exe```.  
3. Open at least one instance of the Edge Browser. If there are no Edge Browser windows open, the tool will remind you to open one.
4. You will see an orange color frame around your Edge window. 
2. Go to Skype and chose *Present programs...* as you normally do.
3. Navigate through the list and find *Edge Presenter* in the list of running apps.
4. You are done.

All windows of the Edge browser will be presented automatically, pretty much like if you would share _Chrome_, _Internet Explorer_ or _Firefox_. If you need more fine grained control of which tabs or web pages are shared, please use the second tool included in the package: **Frame Presenter** 


## Frame Presenter

> Helps to achieve better security by applying one of the main principles in information security: _expose only required amount of information_.  

Utility to share a part of the screen (desktop) in Skype for Business (former Lync).
It helps to overcome the limitation of Skype (Lync), which only allows you to 
share windows of an _individual_ application or _entire_ desktop.
It offers you more fine grained control of what are you sharing and what exactly people can see on the other end of the wire.
For example:
1. During presentation of your new corporate website, you may want to share a part of the web page without showing the temporary site url and your private bookmarks toolbar to the entire world.
2. When sharing _Chrome_ you want to share just one window or tab, regardless of how many Chrome windows you have opened on your Desktop. Without such a tool, you need to make sure you have closed or minimized all irrelevant windows, which makes your life much harder, especially if you need to google for something quickly during the presentation, but not let the other people see it.
3. You want to explain your project structure on the file system. Currently in Skype it's not possible to share Windows Explorer at all. Which makes sense, because you can't hide directory structure and other unwanted files, etc., ... unless you have a _tool_, which allows you to share a fraction of the file system view.
4. Provide your own example... 


#### How to use

1. Build or download ```FramePresenter.exe``` from the release section. 
Releases can be accessed by hovering mouse over *Project overview* menu on the 
left side of the project's main web page. You should see it, if you are reading this. 
2. Launch ```FramePresenter.exe```. 
1. Resize and position the frame on the screen as you like on. 
1. Position applications you would like to share  (or part of them) inside the frame.
2. Go to Skype and chose *Present programs...* as you normally do.
3. Navigate through the list and find *Frame Presenter* in the list of running apps.
4. You are done.

#### Microsoft Windows Edge Browser support

This utility can be also used to solve the problem with sharing Microsoft Edge Browser over Skype, which is a big joke on the market:
> "Standard Windows tool for presenting applications can't not present standard Windows Browser..."

Simply drag the Edge window inside the *Frame Presenter* window and you are done.

#### For advanced users

You can change the level of opacity (or transparency) of the shared content by
passing opacity to the ```FramePresenter.exe``` via command line. 
Example:
```shell
>FramePresenter.exe 96
```
or simply create a shortcut to the executable and adjust the command line parameter.
Valid values are [0..192], where 0 is fully transparent, 192 is almost black.
The bigger values are ignored, because it makes it too dark.

## Build instructions

The project is being developed using Visual Studio 2008. This is partly because of my nostalgia feeling developed over a period of social isolation during COVID-19 pandemic quarantine. 
It can be easily re-compiled with Visual Studio 2017. Just go over standard conversion routine offered by VS2017 and ignore warnings.
The sources can be ```git cloned``` or downloaded as zip file form [gitlab](https://gitlab.com/igorgontar/captureframe/). If ```git clone``` does not work from your corporate network try adjustig the proxy setting in git config.  

License
----------

This is free software under the terms of the MIT license (check the
[COPYING file](/COPYING) included in this package).
