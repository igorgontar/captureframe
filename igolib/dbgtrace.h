#ifndef __IGO_DBGTRACE_H
#define __IGO_DBGTRACE_H

#ifdef _DEBUG
# ifndef	IGO_DBG_TRACE
#  define IGO_DBG_TRACE 1
# endif
#endif

#if(IGO_DBG_TRACE==1)

# define DBG_TRACE(f)					dbg_traces(__FILE__, __LINE__, (f))
# define DBG_TRACE1(f,p1)				dbg_tracesf(__FILE__, __LINE__, (f),(p1))
# define DBG_TRACE2(f,p1,p2)			dbg_tracesf(__FILE__, __LINE__, (f),(p1),(p2))
# define DBG_TRACE3(f,p1,p2,p3)			dbg_tracesf(__FILE__, __LINE__, (f),(p1),(p2),(p3))
# define DBG_TRACE4(f,p1,p2,p3,p4)		dbg_tracesf(__FILE__, __LINE__, (f),(p1),(p2),(p3),(p4))
# define DBG_TRACE5(f,p1,p2,p3,p4,p5)	dbg_tracesf(__FILE__, __LINE__, (f),(p1),(p2),(p3),(p4),(p5))

#else

# define DBG_TRACE(f)					;
# define DBG_TRACE1(f,p1)				;
# define DBG_TRACE2(f,p1,p2)			;
# define DBG_TRACE3(f,p1,p2,p3)			;
# define DBG_TRACE4(f,p1,p2,p3,p4)		;
# define DBG_TRACE5(f,p1,p2,p3,p4,p5)	;

#endif

# define INF_TRACE(f)					dbg_traces(__FILE__, __LINE__, (f))
# define INF_TRACE1(f,p1)				dbg_tracesf(__FILE__, __LINE__, (f),(p1))
# define INF_TRACE2(f,p1,p2)			dbg_tracesf(__FILE__, __LINE__, (f),(p1),(p2))
# define INF_TRACE3(f,p1,p2,p3)			dbg_tracesf(__FILE__, __LINE__, (f),(p1),(p2),(p3))
# define INF_TRACE4(f,p1,p2,p3,p4)		dbg_tracesf(__FILE__, __LINE__, (f),(p1),(p2),(p3),(p4))
# define INF_TRACE5(f,p1,p2,p3,p4,p5)	dbg_tracesf(__FILE__, __LINE__, (f),(p1),(p2),(p3),(p4),(p5))
# define INF_TRACE6(f,p1,p2,p3,p4,p5,p6)	dbg_tracesf(__FILE__, __LINE__, (f),(p1),(p2),(p3),(p4),(p5),(p6))
# define INF_TRACE7(f,p1,p2,p3,p4,p5,p6,p7)	dbg_tracesf(__FILE__, __LINE__, (f),(p1),(p2),(p3),(p4),(p5),(p6),(p7))


void dbg_trace(const char* s);
void dbg_tracef(const char* format, ...);
void dbg_tracefv(const char* format, va_list v);

void dbg_traces(const char* file, int line, const char* msg);
void dbg_tracesf(const char* file, int line, const char* format, ...);
void dbg_tracesfv(const char* file, int line, const char* format, va_list v);

const char* get_short_file_name(const char* file);

#endif // __IGO_DBGTRACE_H
