#include "stdafx.h"
#include "Wnd.h"

namespace igo
{

//---------------------------------------------
// Wnd
//---------------------------------------------
char* Wnd::szClassName = WC_IGOWND;
MSG_MAP_ITEM* Wnd::msgmap = 0;

//-----------------------------------------------------------------
Wnd::Wnd()
{
	m_bFromCreate = 0;
	m_hWnd = 0;       
	m_fnSuclassedWndProc = 0;
}

//-----------------------------------------------------------------
Wnd::~Wnd()
{
	UnSubclass();
	if(m_bFromCreate && m_hWnd)
		DestroyWindow(m_hWnd);
}

//-----------------------------------------------------------------
int Wnd::IsWndClass()
{
	char* cls = this->szClassName;
	return ( strcmp(cls , WC_IGOWND) == 0 );
}

//-----------------------------------------------------------------
HWND Wnd::Create(DWORD dwExStyle, DWORD dwStyle, 
				LPCSTR szClass, LPCSTR szCaption,
				int x, int y, int dx, int dy, HWND hParent, UINT nID, HINSTANCE hInst)
{
	if(hInst==0) hInst = GetModuleHandle(0);
	if(szClass==0) szClass = GetClassName(); 

	WND_THIS_STRUCT ts;
	ts.s = WND_ABRA_CADABRA;
	ts.pThis = this;

	HWND hwnd = CreateWindowEx(dwExStyle, szClass, szCaption, dwStyle, 
		x, y, dx, dy, 
		hParent, (HMENU)nID, hInst, (LPVOID)&ts);
	
	return hwnd;

}

//-----------------------------------------------------------------
Wnd* Wnd::FromHandle(HWND hwnd)
{
	Wnd* pThis = (Wnd*)::GetWindowLong(hwnd, GWL_USERDATA);
	
	if(pThis)
	{ 
#ifdef _DEBUG
		if( pThis->IsWndClass() )
#endif		
			return pThis;
	}
	return 0;
}

//-----------------------------------------------------------------
LRESULT Wnd::DefWndProc(UINT msg, WPARAM wParam, LPARAM lParam)
{
	if(m_fnSuclassedWndProc)
		return CallWindowProc(m_fnSuclassedWndProc, m_hWnd, msg, wParam, lParam);

	return ::DefWindowProc(m_hWnd, msg, wParam, lParam);
}

//-------------------------------------------//
//
//	FUNCTION: find_dword
//
//	searches for the first occurance of the
//  dword in buffer   
//
//	PARAMS:
//		buf - pointer to the buffer of dword's
//		d   - searched dword
//		sz  - buffer size   
//
//-------------------------------------------//

#pragma warning(disable:4035)
unsigned* find_dword(unsigned* buf, unsigned d, unsigned sz)
{
	_asm
	{
		mov		edi,buf				; edi = ptr to buffer
		mov		eax,d				; eax = searched dword
		mov		ecx,sz				; ecx = buffer size
		cld
repne	scasd                   
		sub		edi,00000004		; edi points to searched dword
		cmp     dword ptr[edi],eax	; see if we have a hit
		je      short yes			; yes, we got it
		xor     eax,eax				; no, returning NULL
		je      short no 
yes:	
		mov     eax,edi		 
no:
	}
}
#pragma warning(default:4035)


/*
* debug version
*
*
unsigned* find_dword(unsigned* buf, unsigned d, unsigned sz)
{
	for(unsigned i=0; i<sz; i++)
	{
		if(buf[i]==d)
			return &buf[i];
	}
	return 0;
}
*/

//-------------------------------------------------------------------

//danton
//same as WndProc, but starts handler search from the base class
LRESULT Wnd::WndProcMsgMap(UINT msg, WPARAM wParam, LPARAM lParam, unsigned* pMap)
{
	MSG_MAP_ITEM* pItem = 0;
	unsigned size=0;

	while(pMap)
	{
		size = pMap[1];
		if(size)
		{ 
			pItem = (MSG_MAP_ITEM*)find_dword(pMap + 2, msg, (size<<1));
			if(pItem && pItem->pfn!=0)
				return (this->*(pItem->pfn))(wParam, lParam);
		}
		pMap = (unsigned*)pMap[0];
	}

	return DefWndProc(msg, wParam, lParam);
}

//~danton


//-----------------------------------------------------------------
LRESULT Wnd::WndProc(UINT msg, WPARAM wParam, LPARAM lParam)
{
	return WndProcMsgMap(msg,wParam,lParam,GetMsgMap());
}


//-----------------------------------------------------------------
int Wnd::Subclass(HWND hwnd)
{
	if(m_hWnd || m_bFromCreate || m_fnSuclassedWndProc) return 0;

	SetWindowLong(hwnd, GWL_USERDATA, (long)this);

	m_fnSuclassedWndProc = (WNDPROC)SetWindowLong(hwnd, GWL_WNDPROC, (LONG)GetWndProcAdr());
	if(!m_fnSuclassedWndProc) return 0;

	m_hWnd = hwnd;
	m_bFromCreate = 1;
	return 1;
}

//-----------------------------------------------------------------
int Wnd::UnSubclass()
{
	if(	!m_hWnd			|| 
		!m_bFromCreate	|| 
		!m_fnSuclassedWndProc) 
		return 0;

	SetWindowLong(m_hWnd, GWL_USERDATA, 0);
	SetWindowLong(m_hWnd, GWL_WNDPROC, (LONG)m_fnSuclassedWndProc);

	m_hWnd = 0;
	m_bFromCreate = 0;
	m_fnSuclassedWndProc = 0;
	return 1;
}

} // namespace igo
