//--------------------------------------------------
//
//	IG Template Library
//
//--------------------------------------------------
#ifndef __IGO_COLLECTIONS_H
#define __IGO_COLLECTIONS_H

//-----------------------------------------------------------------------------------
// Stubs for invoking default constructors for template argument types 
//
// NOTE: I moved this out of igo:: namespace because according to new C++ standards
//       the global new operators can only be declared in global namespace
//       now it compiles with VS2017 (haven't tried with CLang and mingw/g++)
//-----------------------------------------------------------------------------------
class void_ptr{ public: void* p; };

inline void* operator new(unsigned s, void_ptr* p)
	{ return p; }

inline void operator delete(void*, void_ptr*){}	//shut up warning C4291 - no matching operator delete found; memory will not be freed if initialization throws an exception

namespace igo
{

#ifndef IGTL_DEF_BLOCK_SIZE						// this parameter is used in
#define IGTL_DEF_BLOCK_SIZE 64					// allocator, lists, hash_set and array
#endif											// to specify the minimum amount of elements
												// allocated by each call to an memory
												// allocation stub
												// 32 mean 32 * sizeof(template <class T>) bytes

#ifndef IGTL_DEF_HASH_TABLE_SIZE				// The default hash table size
#define IGTL_DEF_HASH_TABLE_SIZE	7			// in hash_set
#endif


template<class T>
inline void constructor(T& t)
{
	::new((void_ptr*)&t)T();
}

template<class T>
inline void copy_constructor(const T& from, T& to)
{
	::new((void_ptr*)&to)T(from);
}

//--------------------------------------------
// allocator
//--------------------------------------------
template <class T>
class allocator
{
public:
	allocator();
	allocator(int block_size);
	~allocator(){	free_all(); }

	void* alloc();
	void free(void* p);
	void free_all();
	void set_block_size(int sz);
	int  elem_size() const;
	int  is_empty() const { return (head==0); };

	T * create()
	{
		T * p = (T*)alloc();
		if ( !p )
			return 0;

		try {
			::new ((void_ptr*)p) T();
		} catch ( ... ) {
			free(p);
			throw;
		}
		return p;
	}

	void destroy(T* p)
	{
		if ( !p )
			return;
		try {
			p->~T();
		} catch ( ... ) {
			free(p);
			throw;
		}
	}


	struct block{
		unsigned id;
		block* next;
		block* prev;
		int    count;
		int    size;
	};

protected:
	block* head;
	int    block_size;	// block is : n - elements of elem_size
};




//--------------------------------------------
// array
//--------------------------------------------
template<class T>
class array
{
public:
	array();

	array(const array<T>& arr);
	array<T>& operator=(const array<T>& arr);

	~array(){ remove_all(); }
	int set_size(int n, int bCallConstructor = 1);	// return new size or -1, if fails
	int insert(int i, const T& p);					// return i or, -1 if fails
	int remove(int i);								// return i or, -1 if fails
	void remove_all()		{ set_size(0); }

	T* get_element(int i){ return (T*)data + i; }
	T& operator[](int i){ return *get_element(i); }
	const T* get_element(int i) const { return (const T*)data + i; }
	const T& operator[] (int i) const { return *get_element(i); }

	operator T* ()			{ return (T*)data; }
	operator const T* () const { return (const T*)data; }
	int get_size() const { return size; }
	void set_block_size(int sz);
	T* forget();

	int steal(array<T>& arr);

protected:
	void* data;	// declaring as first member to have the same address with this
	int   size;
	int   max_size;
	int   block_size;
};

class void_ptr_array : public array<void*> {};

//--------------------------------------------
// typed_ptr_array
//--------------------------------------------
template<class T>
class typed_ptr_array : public void_ptr_array
{
public:
	typed_ptr_array(){}
	~typed_ptr_array(){}
	int insert(int i, const T*& p)	{ return void_ptr_array::insert(i, (void*)p); }
	T** get_element(int i)			{ return (T**)void_ptr_array::get_element(i); }
	T*& operator[](int i)			{ return *get_element(i); };

	T* const * get_element(int i) const 	{ return (T* const *)void_ptr_array::get_element(i); }
	T* operator[] (int i) const 	{ return *get_element(i); };

};

//--------------------------------------------
// auto_ptr_array
//--------------------------------------------
template<class T>
class auto_ptr_array : public typed_ptr_array<T>
{

private:
	auto_ptr_array(const auto_ptr_array<T>& ) {} //prohibit copy ctor

public:
	auto_ptr_array(){}
	~auto_ptr_array(){ remove_all(); }
	int remove(int i);
	void remove_all();
};


//------------------------------------------------------------------------
// template<class T> class foxy_list
//
// The simplest list implementation
// especialy designed for memory alocation optimization reason :
//	 - uses static allocator of the list elements with tunable block size (edit 2020: not true anymore) 
//   - when included as data member	it takes only 2*sizeof(void*) bytes
//-------------------------------------------------------------------------
template<class T>
class foxy_list
{
public:
	foxy_list() : head(0), size(0)	{ set_block_size(IGTL_DEF_BLOCK_SIZE); }
	~foxy_list()										{ remove_all(); }

	void set_block_size(int sz)
	{
		if(sz<1) sz = IGTL_DEF_BLOCK_SIZE;
		mem.set_block_size(sz);
	}

	T* attach(T* prev, T* p)
	{
		if(prev==p) return 0;
		if(prev)	{
			((elem*)p)->next = ((elem*)prev)->next;
			((elem*)prev)->next = (elem*)p;
		} else {
			((elem*)p)->next = head;
			head = (elem*)p;
		}
		++size;
		return p;
	}

	T* dettach(T* prev, T* value)
	{
		if(prev==value) return 0;
		if(prev) {
			((elem*)prev)->next = ((elem*)value)->next;
		} else {
			head = ((elem*)value)->next;
		}
		((elem*)value)->next = 0;
		--size;
		return value;
	}

	T* insert(T* prev) 	// return 0 if fails, if prev==0 insert at head
	{
		//{ elem* p = new elem;
		elem* p = (elem*)mem.alloc();
		if(!p) return 0;
		constructor(p->value);
		//}
		return attach(prev, (T*)p);
	}

	int remove(T* value) {
		T* prev = get_prev(value);
		return remove(prev, value);
	}

	int remove(T* prev, T* value)
	{
		elem* p = ((elem*)dettach(prev, value));
		//{delete p;
		((T*)(void*)(&p->value))->~T();
		mem.free(p);
		//}
		return 0;
	}

	void remove_all()
	{
		elem* p = head;
		while(p) {
			elem* t = p->next;
			//{ delete p;
			((T*)(void*)(&p->value))->~T();
			mem.free(p);
			//}
			p = t;
		}
		head = 0;
		size = 0;
	}

	int get_size()	const	{ return size; }
	int is_empty()	const	{ return (head==0); }
	T* get_head()			{ return (T*)head; }
	T* get_next(T* value)	{ return (T*)(((elem*)value)->next); }
	T* get_prev(T* value)
	{
		T* prev = 0, *p = (T*)head;
		while(p) {
			if(p==value)
				return prev;
			prev = p;
			p = get_next(p);
		}
		return 0;
	}

	struct elem {
		T value;
		elem* next;
	};

protected:
	elem*		head;
	int			size;
//	static allocator<elem>	mem;   // problem with shared static allocator is the it is not thread safe
	allocator<elem>	mem;		   // problem with private allocator is that element can't be moved from one list to another by reference, deep copy is required
};

//----------------------------------------------
// List with search capabilities
//----------------------------------------------
template<class T, class Key, class KeyOfT, class KeyCompare>
class foxy_find_list : public foxy_list<T>
{
public:
	typedef KeyCompare	key_compare;
	typedef KeyOfT			key_of_type;
	T* find(Key key, T** previous=0)
	{
		T* p = foxy_list<T>::get_head();
		T* prev = 0;
		while(p) {
			if( key_compare()(key, key_of_type()(p)) ) {
				if(previous)
					*previous = prev;
				return p;
			}
			prev = p;
			p = get_next(p);
		}
		if(previous)
			*previous = prev;
		return 0;
	}
};

class void_ptr_llist : public foxy_list<void*> {};

//--------------------------------------------
// typed_ptr_llist
//--------------------------------------------
template<class T>
class typed_ptr_llist : public void_ptr_llist
{
public:
	typed_ptr_llist(){}
	~typed_ptr_llist(){}

	T** insert(T** after)	{ return (T**)void_ptr_llist::insert((void**)after); }
	int remove(T** value)	{ return void_ptr_llist::remove((void**)value); }
	T** get_head()			{ return (T**)void_ptr_llist::get_head(); }
	T** get_next(T** value)	{ return (T**)void_ptr_llist::get_next((void**)value); }
};


//--------------------------------------------
// auto_ptr_llist
//--------------------------------------------
template<class T>
class auto_ptr_llist : public typed_ptr_llist<T>
{
public:
	auto_ptr_llist(){}
	~auto_ptr_llist(){ remove_all(); }
	int remove(T** value);
	void remove_all();
};

//------------------------------------------------------------------------
// template<class T> class dl_list
//
// double-linked list
//-------------------------------------------------------------------------
template<class T>
class dllist
{
public:
	dllist() : head(0), tail(0), size(0)	{ set_block_size(IGTL_DEF_BLOCK_SIZE); }
	~dllist()								{ remove_all(); }

	void set_block_size(int sz)
	{
		if(sz<1) sz = IGTL_DEF_BLOCK_SIZE;
		mem.set_block_size(sz);
	}

	T* attach(T* prev, T* p)
	{
		if(prev==p) return 0;
		if(prev)	{
			((elem*)p)->next = ((elem*)prev)->next;
			((elem*)prev)->next = (elem*)p;
		} else {
			((elem*)p)->next = head;
			head = (elem*)p;
		}

		if(((elem*)p)->next)
			((elem*)p)->next->prev = ((elem*)p);

		((elem*)p)->prev = ((elem*)prev);

		if((elem*)prev==tail)
			tail = (elem*)p;

		++size;
		return p;
	}

	T* dettach(T* prev, T* p)
	{
		if(prev==p) return 0;
		if(prev) {
			((elem*)prev)->next = ((elem*)p)->next;
		} else {
			head = ((elem*)p)->next;
		}

		if(((elem*)p)->next)
			((elem*)p)->next->prev = ((elem*)prev);
		else
			tail=(elem*)prev;

		((elem*)p)->next = 0;
		((elem*)p)->prev = 0;

		--size;
		return p;
	}

	T* insert(T* prev) 	// return 0 if fails, if prev==0 insert at head
	{
		//{ elem* p = new elem;
		elem* p = (elem*)mem.alloc();
		if(!p) return 0;
		constructor(p->value);
		//}
		return attach(prev, (T*)p);
	}

	int remove(T* value) {
		T* prev = get_prev(value);
		return remove(prev, value);
	}

	int remove(T* prev, T* value)
	{
		elem* p = ((elem*)dettach(prev, value));
		//{delete p;
		((T*)(void*)(&p->value))->~T();
		mem.free(p);
		//}
		return 0;
	}

	void remove_all()
	{
		elem* p = head;
		while(p) {
			elem* t = p->next;
			//{ delete p;
			((T*)(void*)(&p->value))->~T();
			mem.free(p);
			//}
			p = t;
		}
		head = 0;
		tail = 0;
		size = 0;
	}

	int get_size() const	{ return size; }
	int is_empty() const	{ return (head==0); }
	T* get_head()			{ return (T*)head; }
	T* get_tail()			{ return (T*)tail; }
	T* get_next(T* value)	{ return (T*)(((elem*)value)->next); }
	T* get_prev(T* value)	{ return (T*)(((elem*)value)->prev); }
    T* attach_tail(T* p)    { return attach(get_tail(), p); }
    T* attach_head(T* p)    { return attach(get_head(), p); }
	T* dettach(T* p)        { return dettach(get_prev(p), p); }

	struct elem {
		T value;
		elem* next;
		elem* prev;
	};

protected:
	elem*		head;
	elem*		tail;
	int			size;
//	static allocator<elem>	mem;
	allocator<elem>	mem;
};

//--------------------------------------------
// hash_set
//--------------------------------------------
template<class TKEY, class TVAL, class KeyOfVal, class Compare>
class hash_set
{
public:
	typedef Compare		key_compare;
	typedef KeyOfVal	key_of_val;

	hash_set();
	~hash_set();

	int  init(unsigned table_size);		// return 0 if error
	void set_block_size(int sz);
	TVAL* insert(const TKEY& key, int& bExist);
																		// return  0 : error
																		//        !0 : ptr to existing (bExist!=0) or newly inserted (bExist==0) value
	TVAL* insert_at(const TKEY& key, unsigned hash_key);
																		// !!!For power users!!!
																		// the same but use precalculated hash_key
																		// for using during backup/restore to/from stream
																		// NOTE: 1) be sure that insertable key is unique within
																		//          all keys (with the same hash_key) inserted before.
                                                                        //       2) while restoring, the table_size (set through init())
                                                                        //          should be the same
                                                                        //          as during backup

	TVAL* find(const TKEY& key);			// return 0 if not exist
	int  remove(const TKEY& key);			// return 0 if not exist
	void remove_all();

	unsigned  get_size() const	{ return size; }
	unsigned  get_table_size() const { return table_size; }

	void* get_first_position();
	TVAL* get_next(void*& pos, unsigned& hash_key);

protected:

	struct asoc	{
		TVAL  value;
		asoc*	next;
	};

	asoc**   table;	 // pointer to the head of hash table
	unsigned table_size; // hash table size
	unsigned size;	 // nomber of elements in map
	asoc*    piter;	 // for iteration
//	static allocator<asoc> mem;
	allocator<asoc> mem;
};

template <class TKEY>
class key_itself
{
public:
	const TKEY& operator()(const TKEY& p) {return p;}
};

template <class TKEY>
class generic_compare
{
public:
	int operator()(const TKEY& p1,const TKEY& p2) {return p1==p2;}
};

//--------------------------------------------
// key_set - just the set of single values
//--------------------------------------------
template <class TKEY>
class key_set : public hash_set<TKEY,TKEY,key_itself<TKEY>,generic_compare<TKEY> >
{
public:
	key_set<TKEY>() : hash_set<TKEY,TKEY,key_itself<TKEY>,generic_compare<TKEY> >() {}
};

//--------------------------------------------
// stack_fix
//--------------------------------------------
template<class T>
class fixed_stack
{
public:
	fixed_stack()				{ reset(); }
	~fixed_stack()			{ free(); }
	int get_size() const	{ return (top-base); }
	int get_buf_size() const	{ return (tail-base); }
	void free()					{ if(base){ ::free(base); reset();} }
	void empty()				{ top=base; }
	int is_full() const			{ return (top>=tail); }
	T&	pop()						{ return *--top; }
	void push(const T& v)	{ *top++ = v; }
	T& operator[](int i) { return top[i-1]; }
	T& operator()(int i) { return base[i]; }
	operator T*()				{ return base; }

	int set_size(int size)	{
		if(size<0) return -1;
		if(size==0) { free();	return 0; }

		int old_size = get_buf_size();
		if(size > old_size) {
			if(base)
//				base = (T*)realloc(base, size*sizeof(T), old_size*sizeof(T));
				base = (T*)realloc(base, size*sizeof(T));
			else
				base = (T*)malloc(size*sizeof(T));
			if(!base)
				return -1;
		}
		tail = base + size;
		empty();
		return size;
	}

protected:
	T* base;
	T* top;
	T* tail;

	void reset(){ base=top=tail=0; }
};

//--------------------------------------------
// array
//--------------------------------------------
template<class T>
inline array<T>::array()
{
	size = 0;
	data = 0;
	max_size = 0;
	block_size = IGTL_DEF_BLOCK_SIZE;
}


#define DUMB

template<class T>
inline array<T>::array(const array<T>& arr)
{
	size = 0;
	data = 0;
	max_size = 0;
	block_size = arr.block_size;

#ifdef DUMB
	operator=(arr);

#else
	if ( set_size(arr.size,false) >= 0 )
	{
		T* pDst=(T*)data;
		T* pSrc=(T*)arr.data;
		for ( int i = 0; i < size; ++i )
			copy_constructor(pSrc[i], pDst[i]);
	}
	else
	{
		throw -1;
	}
#endif
}

template<class T>
inline array<T>& array<T>::operator=(const array<T>& arr)
{
#ifdef DUMB
	set_size(arr.size);

	T* pDst=(T*)data;
	T* pSrc=(T*)arr.data;

	for ( int i = 0; i < arr.size; ++i )
	{
		pDst[i] = pSrc[i];
	}

#else


	for ( int i = 0; i < size; ++i )
		get_element(i)->~T();

	block_size = arr.block_size;

	if ( arr.size == 0 )
	{
		free(data);
		data = 0;
	}
	else
	{
		size = max_size = arr.size;
		data = realloc(data, max_size * sizeof(T));
	}

	if ( !data )
	{
		size = 0;
		max_size = 0;
	}

	T* pDst=(T*)data;
	T* pSrc=(T*)arr.data;

	for ( /*int */i = 0; i < size; ++i )
		copy_constructor(pSrc[i], pDst[i]);
#endif


	return *this;
}


template<class T>
inline int array<T>::steal(array<T>& arr)
{
	set_size(0);
	data = arr.data;
	size = arr.size;
	max_size = arr.max_size;
	block_size = arr.block_size;

	arr.data = 0;
	arr.size = 0;
	arr.max_size = 0;

	return size;
}



template<class T>
inline void array<T>::set_block_size(int sz)
{
	if(sz<1) sz = IGTL_DEF_BLOCK_SIZE;
	block_size = sz;
}

template<class T>
inline T* array<T>::forget()
{
	T* p=(T*)data;
	data=0;
	size=max_size=0;
	return p;
}

template<class T>
inline int array<T>::set_size(int n, int bCallConstructor)
{
	int i = 0;
	for(i=n; i<size; i++)
		get_element(i)->~T();

	if(n==0)
	{
		if(data)
		{
			free(data);
			data = 0;
			size = 0;
			max_size = 0;
		}
		return 0;
	}

	//int prev_max_size =  max_size;
	if(!data)
	{
		int c = n/block_size;
		if( (n % block_size) != 0) c++;
		max_size = (c * block_size);
		data = malloc( max_size * sizeof(T) );
	}else{
		if(n > max_size)
		{
			int c = (n-max_size)/block_size;
			if( ((n-max_size) % block_size) != 0) c++;

			max_size += (c * block_size);

//			data = realloc(data, max_size * sizeof(T), prev_max_size * sizeof(T));
			data = realloc(data, max_size * sizeof(T));
		}
	}

	if(!data)
	{
		size = max_size = 0;
		return -1;
	}

	if(bCallConstructor)
	{
		for(i=size; i<n; i++)
		{
			T& t = operator[](i);
			constructor( t );
		}
	}

	return (size = n);
}

template<class T>
inline int array<T>::insert(int i, const T& p)
{
	if(i>=0 && i<=size)
	{
		int oldsize = size;
		if( set_size(size + 1, 0) < 0 ) // allocate without calling constructors
			return -1;
		memmove( get_element(i+1), get_element(i), (oldsize - i) * sizeof(T) );

		T& t = operator[](i);
		constructor( t );
		operator[](i) = p;

		return i;
	}
	return -1;
}

template<class T>
inline int array<T>::remove(int i)
{
	if(data && i>=0 && i<size)
	{
		int oldsize = size;
		get_element(i)->~T();
		memmove( get_element(i), get_element(i+1), (size - i - 1) * sizeof(T) );
		size--;
		set_size(size, 0);
		return i;
	}
	return -1;
}

//--------------------------------------------
// auto_ptr_array
//--------------------------------------------
template<class T>
inline int auto_ptr_array<T>::remove(int i)
{
	delete typed_ptr_array<T>::operator[](i);
	return typed_ptr_array<T>::remove(i);
}

template<class T>
inline void auto_ptr_array<T>::remove_all()
{
	for(int i=0; i<typed_ptr_array<T>::size; i++) {
		delete typed_ptr_array<T>::operator[](i);
	}
	typed_ptr_array<T>::remove_all();
}

//--------------------------------------------
// auto_ptr_llist
//--------------------------------------------
template<class T>
int auto_ptr_llist<T>::remove(T** value)
{
	delete (*value);
	return typed_ptr_llist<T>::remove(value);
}
/*
template<class T>
void auto_ptr_llist<T>::remove_all()
{
	foxy_list<T>::elem* p;
	for(p = foxy_list<T>::head; p; p = p->next) {
		delete (p->value);
	}
	typed_ptr_llist<T>::remove_all();
}
*/
//--------------------------------------------
// allocator
//--------------------------------------------
#define BLOCK_ID 0xffaaddcc

template <class T>
inline allocator<T>::allocator()
{
	head = 0;
	block_size = IGTL_DEF_BLOCK_SIZE;
}

template <class T>
inline allocator<T>::allocator(int block_size)
{
	head = 0;
	set_block_size(block_size);
}

template <class T>
inline void allocator<T>::set_block_size(int sz)
{
	if(sz<1) sz = IGTL_DEF_BLOCK_SIZE;
	block_size = sz;
}

template <class T>
inline int allocator<T>::elem_size() const
{
	return (sizeof(T) + sizeof(unsigned));
}

template <class T>
inline void allocator<T>::free_all()
{
	block* p = head;
	while(p)
	{
		block* next = p->next;
		delete [] ((char*)p);
		p = next;
	}
	head=0;
}

template <class T>
inline void* allocator<T>::alloc()
{
	if(head==0 || block_size - head->size < 1)
	{   // allocating new block

		block* p = (block*)new char [sizeof(block) + block_size * elem_size()];
		if(!p) return 0;

		p->id = BLOCK_ID;

		p->prev = 0;
		p->next = head;
		if(head)
			head->prev = p;
		head = p;

		char* data = (char*)p + sizeof(block);
		*((unsigned*)data) = (unsigned)p;
		p->count = 1;
		p->size	 = 1;
		data += sizeof(unsigned);
		return data;

	}else{	// using existing space
		char* p = (char*)head + sizeof(block) + head->size * elem_size();
		head->count++;
		head->size++;
		*((unsigned*)p) = (unsigned)head;
		p += sizeof(unsigned);
		return p;
	}

	return 0;
}

template <class T>
inline void allocator<T>::free(void* t)
{
	// determining to which block belongs t
	block* p = (block*)(*((unsigned*)t - 1));
	if(p->id != BLOCK_ID)
	{
		throw int(0);
		return;
	}

	p->count--;

	if(p->count==0)
	{
		block* prev = p->prev;
		if(prev)
			prev->next = p->next;
		else
			head = p->next;

		if(p->next)
			p->next->prev = prev;

		delete [] ((char*)p);
		return;

	}else if(p->count<0){

		throw int(0);
	}
	return;
}

//--------------------------------------------
// hash_set
//--------------------------------------------


//----------------------------------------------------
// has_key() returns a hash value for a string
// Taken from Aho Sethi Ullman Compilers book.
//----------------------------------------------------
/*
inline unsigned hash_key(const char* s)
{
  unsigned int h = 0, g;
  const char *p;
  for(p=s; *p; p++)
  {
      h = (h << 4) + *p;
      if( (g = h & 0xf0000000) )
			{
				h = h ^ (g >> 24);
				h = h ^ g;
			}
  }
  return h;
}
*/

//--------------------------------------------
// Microsoft version of hash key for string
// Taken from the Template part of MFC
//--------------------------------------------
inline unsigned hash_key(const char* s)
{
	register const char* key = s;
	register unsigned nHash = 0;
	while(*key)
		nHash = (nHash<<5) + nHash + *key++;
	return nHash;
}


template<class TKEY>
inline unsigned hash_key(const TKEY& key)
{
	return (((unsigned)key) >> 4);
}

template<class TKEY, class TVAL, class KeyOfVal, class Compare>
inline hash_set<TKEY,TVAL,KeyOfVal,Compare>::hash_set()
{
	size		= 0;
	table		= 0;
	table_size	= 0;
	init(IGTL_DEF_HASH_TABLE_SIZE);
}

template<class TKEY, class TVAL, class KeyOfVal, class Compare>
inline hash_set<TKEY,TVAL,KeyOfVal,Compare>::~hash_set()
{
	remove_all();
	if(table)
		delete [] ((char*)table);
}

template<class TKEY, class TVAL, class KeyOfVal, class Compare>
void hash_set<TKEY,TVAL,KeyOfVal,Compare>::set_block_size(int sz)
{
	if(sz<1) sz = IGTL_DEF_BLOCK_SIZE;
	mem.set_block_size(sz);
}

template<class TKEY, class TVAL, class KeyOfVal, class Compare>
inline int  hash_set<TKEY,TVAL,KeyOfVal,Compare>::init(unsigned tsize)
{
	if(size)		// can't init hash table when some elements present	in map
		return 0;

	if(table && table_size != tsize)
	{
		delete [] ((char*)table);
		table=0;
	}

	if(!table)
		table = (asoc**)new char[tsize*sizeof(asoc*)];

	if(table)
	{
		table_size = tsize;
		memset(table, 0, tsize*sizeof(asoc*));
		return 1;
	}
	return 0;
}

template<class TKEY, class TVAL, class KeyOfVal, class Compare>
inline TVAL* hash_set<TKEY,TVAL,KeyOfVal,Compare>::insert(const TKEY& key, int& bExist)
{
	asoc*  p;
	asoc** pp = table + ::hash_key(key) % table_size;

	for(p = *pp; p; p = p->next)
	{
		if( key_compare()(key, key_of_val()(p->value)) )
		{
			bExist = 1;
			return &p->value;
		}
	}

	// not found adding new
	p = (asoc*)mem.alloc();
	if(p)
	{
		constructor(p->value);
		size++;

		p->next = *pp;
		*pp = p;

		bExist = 0;
		return &p->value;
	}

	return 0;
}

template<class TKEY, class TVAL, class KeyOfVal, class Compare>
inline TVAL* hash_set<TKEY,TVAL,KeyOfVal,Compare>::find(const TKEY& key)
{
	asoc* p;
	asoc** pp = table + ::hash_key(key) % table_size;
	for(p = *pp; p; p = p->next)
	{
		if( key_compare()(key, key_of_val()(p->value)) )
			return &p->value;
	}
	return 0;
}


template<class TKEY, class TVAL, class KeyOfVal, class Compare>
inline int hash_set<TKEY,TVAL,KeyOfVal,Compare>::remove(const TKEY& key)
{
	asoc* p, *pPrev;
	asoc** pp = table + ::hash_key(key) % table_size;
	for(pPrev = p = *pp; p; pPrev = p, p = p->next)
	{
		if( key_compare()(key, key_of_val()(p->value)) )
		{
			pPrev->next = p->next;
			if(*pp == p) // if it is head element
				*pp = p->next;

			((TVAL*)&p->value)->~TVAL();

			mem.free(p);
			size--;
			return 1;
		}
	}
	// not found
	return 0;
}

template<class TKEY, class TVAL, class KeyOfVal, class Compare>
inline void hash_set<TKEY,TVAL,KeyOfVal,Compare>::remove_all()
{
	if(!table || size==0)
		return;

	asoc*  p;
	asoc** pp;
	asoc** ppLast = table + table_size;

	for(pp=table; pp<ppLast; pp++)
	{
		for(p = *pp; p; )
		{
			asoc* next = p->next;
			((TVAL*)&p->value)->~TVAL();
			mem.free(p);
			p = next;
		}
	}
	memset(table, 0, table_size*sizeof(asoc*));
	size=0;
}

template<class TKEY, class TVAL, class KeyOfVal, class Compare>
inline TVAL* hash_set<TKEY,TVAL,KeyOfVal,Compare>::insert_at(const TKEY& key, unsigned hash_key)
{
	asoc*  p;
	asoc** pp = table + hash_key % table_size;

	p = (asoc*)mem.alloc();
	if(p)
	{
		constructor(p->value);
		size++;
		p->next = *pp;
		*pp = p;
		return &p->value;
	}
	return 0;
}

template<class TKEY, class TVAL, class KeyOfVal, class Compare>
inline void* hash_set<TKEY,TVAL,KeyOfVal,Compare>::get_first_position()
{
	if(!table || size==0)
		return 0;

	asoc** pp;
	asoc** ppLast = table + table_size;
	for(pp = table; *pp==0 && pp < ppLast; pp++)
	{}

	if(pp==ppLast) // finish
	{
		piter = 0;
		return 0;
	}

	piter = *pp;
	return pp;
}

template<class TKEY, class TVAL, class KeyOfVal, class Compare>
inline TVAL* hash_set<TKEY,TVAL,KeyOfVal,Compare>::get_next(void*& pos, unsigned& hash_key)
{
	if(!pos) return 0;
	asoc** pp;
	asoc** ppFirst = (asoc**)pos;
	asoc** ppLast = table + table_size;
	TVAL* pval = 0;

	if(piter)	{
		pval = &piter->value;
		hash_key = ppFirst - table;
		piter = piter->next;
	}

	if(piter==0) { // last asoc with this hash_key

		for(pp = ppFirst+1; *pp==0 && pp < ppLast; pp++)
		{}

		if(pp==ppLast) // finish
		{
			pos = 0;
			piter = 0;
		}else{
			pos = pp;
			piter = *pp;
		}
	}
	return pval;
}

} // namespace igo

#endif //__IGO_COLLECTIONS_H

