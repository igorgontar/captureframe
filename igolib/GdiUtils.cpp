#include "stdafx.h"
#include "GdiUtils.h"

static HBITMAP s_hBmp   = 0;
static HBRUSH  s_hBrush = 0;

// automatic destructor
static struct AUTO_FREE
{
	~AUTO_FREE()
	{
		DeInitGdiUtils();
	}
} autofree;



int	InitGdiUtils()
{
	if(!s_hBmp) {
		unsigned short data[] = {0xaaaa,0x5555,0xaaaa,0x5555,0xaaaa,0x5555,0xaaaa,0x5555};
		s_hBmp = CreateBitmap(8, 8, 1, 1, data);
		if(!s_hBmp)
			return 0;
	}
	if(!s_hBrush) {
		s_hBrush = CreatePatternBrush(s_hBmp);
		if(!s_hBrush)
			return 0;
	}
	return 1;
}

void DeInitGdiUtils()
{
	if(s_hBmp) {
		DeleteObject(s_hBmp);
		s_hBmp = 0;
	}
	if(s_hBrush) {
		DeleteObject(s_hBrush);				
		s_hBrush = 0;
	}
}

void DrawHatchRect1(HDC hdc, int x, int y, int cx, int cy)
{
	HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, s_hBrush);
	PatBlt(hdc, x, y, cx, cy, PATINVERT);
	if(hOldBrush)
		SelectObject(hdc, hOldBrush);
}

void DrawHatchRect2(HDC hdc, int x, int y, int cx, int cy)
{
	HBRUSH hOldBrush = (HBRUSH)SelectObject(hdc, s_hBrush);
	PatBlt(hdc, x, y, cx, cy, PATCOPY);
	if(hOldBrush)
		SelectObject(hdc, hOldBrush);
}

void FillSolidRect(HDC hdc, LPCRECT lpRect, COLORREF clr)
{
	::SetBkColor(hdc, clr);
	::ExtTextOut(hdc, 0, 0, ETO_OPAQUE, lpRect, NULL, 0, NULL);
}

void FillSolidRect(HDC hdc, int x, int y, int dx, int dy, COLORREF clr)
{
   RECT rc; rc.left=x; rc.top=y; rc.right=x+dx; rc.bottom=y+dy;
   FillSolidRect(hdc, &rc, clr);
}

void Draw3DRect(HDC hdc, int x, int y, int cx, int cy, COLORREF clrTopLeft, COLORREF clrBottomRight)
{
	FillSolidRect(hdc, x, y, cx - 2, 2, clrTopLeft);
	FillSolidRect(hdc, x, y, 1, cy - 2, clrTopLeft);
	FillSolidRect(hdc, x + cx, y, -2, cy, clrBottomRight);
	FillSolidRect(hdc, x, y + cy, cx, -2, clrBottomRight);
}

void Draw3DRect(HDC hdc, LPCRECT lpRect,
	COLORREF clrTopLeft, COLORREF clrBottomRight)
{
	Draw3DRect(hdc, lpRect->left, lpRect->top, lpRect->right - lpRect->left,
		lpRect->bottom - lpRect->top, clrTopLeft, clrBottomRight);
}

void DrawSplitBar(HDC hdc, RECT* prc)
{
	RECT rc = *prc;
   	COLORREF c3dface = GetSysColor(COLOR_3DFACE);
   	COLORREF c3dshadow = GetSysColor(COLOR_3DSHADOW);
   	COLORREF c3ddkshadow = GetSysColor(COLOR_3DDKSHADOW);
   	COLORREF c3dhilight = GetSysColor(COLOR_3DHILIGHT);

	// 1
	SetBkColor(hdc, c3dface);
	rc = *prc;
	rc.left++ ; rc.right--; rc.bottom = rc.top + 1;
	ExtTextOut(hdc, rc.left, rc.top, ETO_OPAQUE, &rc, NULL, 0, NULL);
	SetPixel(hdc, prc->left, prc->top, c3dshadow);

	// 2
	rc = *prc;
	rc.top++; rc.bottom = rc.top + 1;
	SetBkColor(hdc, c3dhilight);
	ExtTextOut(hdc, rc.left, rc.top, ETO_OPAQUE, &rc, NULL, 0, NULL);

	// plato
	rc = *prc;
	rc.top += 2; rc.bottom -= 2;
	SetBkColor(hdc, c3dface);
	ExtTextOut(hdc, rc.left, rc.top, ETO_OPAQUE, &rc, NULL, 0, NULL);

	// 3
	rc = *prc;
	rc.left++; rc.top = prc->bottom-2; rc.right--; rc.bottom = rc.top + 1;
	SetBkColor(hdc, c3dshadow);
	ExtTextOut(hdc, rc.left, rc.top, ETO_OPAQUE, &rc, NULL, 0, NULL);


	// 4
	rc = *prc;
	rc.left++; rc.top = prc->bottom-1; rc.right-=2; rc.bottom = rc.top + 1;
	SetBkColor(hdc, c3ddkshadow);
	ExtTextOut(hdc, rc.left, rc.top, ETO_OPAQUE, &rc, NULL, 0, NULL);
}
