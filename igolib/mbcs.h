#ifndef _MBCS_H_INCLUDED
#define _MBCS_H_INCLUDED
#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <mbstring.h>

inline int mbbtombc(int c) { return _mbbtombc(c); }
inline int mbbtype(char c, int t) { return _mbbtype(c, t); }
inline int mbctombb(int c) { return _mbctombb(c); }
inline int mbsbtype(const char *s, size_t n) { return _mbsbtype((const unsigned char *)s, n); }
inline char* mbscat(char * s1, const char * s2) { return (char*)_mbscat((unsigned char *)s1, (const unsigned char *)s2); }
inline char* mbschr(const char * s, int c) { return (char*)_mbschr((const unsigned char *)s, c); }
inline int mbscmp(const char * s1, const char * s2) { return _mbscmp((const unsigned char *)s1, (const unsigned char *)s2); }
inline int mbscoll(const char * s1, const char * s2) { return _mbscoll((const unsigned char *)s1, (const unsigned char *)s2); }
inline char* mbscpy(char * s1, const char * s2) { return (char*)_mbscpy((unsigned char *)s1, (const unsigned char *)s2); }
inline size_t mbscspn(const char * s1, const char * s2) { return _mbscspn((const unsigned char *)s1, (const unsigned char *)s2); }
inline char* mbsdec(const char * s1, const char * s2) { return (char*)_mbsdec((const unsigned char *)s1, (const unsigned char *)s2); }
inline char* mbsdup(const char * s) { return (char*)_mbsdup((const unsigned char *)s); }
inline int mbsicmp(const char * s1, const char * s2) { return _mbsicmp((const unsigned char *)s1, (const unsigned char *)s2); }
inline int mbsicoll(const char * s1, const char * s2) { return _mbsicoll((const unsigned char *)s1, (const unsigned char *)s2); }
inline char* mbsinc(const char * s) { return (char*)_mbsinc((const unsigned char *)s); }
inline size_t mbslen(const char * s) { return _mbslen((const unsigned char *)s); }
inline char* mbslwr(char * s) { return (char*)_mbslwr((unsigned char *)s); }
inline char* mbsnbcat(char * s1, const char * s2, size_t n) { return (char*)_mbsnbcat((unsigned char *)s1, (const unsigned char *)s2, n); }
inline int mbsnbcmp(const char * s1, const char * s2, size_t n) { return _mbsnbcmp((const unsigned char *)s1, (const unsigned char *)s2, n); }
inline int mbsnbcoll(const char * s1, const char * s2, size_t n ) { return _mbsnbcoll((const unsigned char *)s1, (const unsigned char *)s2, n); }
inline size_t mbsnbcnt(const char * s, size_t n) { return _mbsnbcnt((const unsigned char *)s, n); }
inline char* mbsnbcpy(char * s1, const char * s2, size_t n) { return (char*)_mbsnbcpy((unsigned char *)s1, (const unsigned char *)s2, n); }
inline int mbsnbicmp(const char * s1, const char * s2, size_t n) { return _mbsnbicmp((const unsigned char *)s1, (const unsigned char *)s2, n); }
inline int mbsnbicoll(const char * s1, const char * s2, size_t n) { return _mbsnbicoll((const unsigned char *)s1, (const unsigned char *)s2, n); }
inline char* mbsnbset(char * s, int c, size_t n) { return (char*)_mbsnbset((unsigned char *)s, c, n); }
inline char* mbsncat(char * s1, const char * s2, size_t n) { return (char*)_mbsncat((unsigned char *)s1, (const unsigned char *)s2, n); }
inline size_t mbsnccnt(const char * s, size_t n) { return _mbsnccnt((const unsigned char *)s, n); }
inline int mbsncmp(const char * s1, const char * s2, size_t n) { return _mbsncmp((const unsigned char *)s1, (const unsigned char *)s2, n); }
inline int mbsncoll(const char * s1, const char * s2, size_t n) { return _mbsncoll((const unsigned char *)s1, (const unsigned char *)s2, n); }
inline char* mbsncpy(char * s1, const char * s2, size_t n) { return (char*)_mbsncpy((unsigned char *)s1, (const unsigned char *)s2, n); }
inline int mbsnextc (const char * s) { return _mbsnextc ((const unsigned char *)s); }
inline int mbsnicmp(const char * s1, const char * s2, size_t n) { return _mbsnicmp((const unsigned char *)s1, (const unsigned char *)s2, n); }
inline int mbsnicoll(const char * s1, const char * s2, size_t n) { return _mbsnicoll((const unsigned char *)s1, (const unsigned char *)s2, n); }
inline char* mbsninc(const char * s, size_t n) { return (char*)_mbsninc((const unsigned char *)s, n); }
inline char* mbsnset(char * s, int c, size_t n) { return (char*)_mbsnset((unsigned char *)s, c, n); }
inline char* mbspbrk(const char * s1, const char * s2) { return (char*)_mbspbrk((const unsigned char *)s1, (const unsigned char *)s2); }
inline char* mbsrchr(const char * s, int c) { return (char*)_mbsrchr((const unsigned char *)s, c); }
inline char* mbsrev(char * s) { return (char*)_mbsrev((unsigned char *)s); }
inline char* mbsset(char * s, int c) { return (char*)_mbsset((unsigned char *)s, c); }
inline size_t mbsspn(const char * s1, const char * s2) { return _mbsspn((const unsigned char *)s1, (const unsigned char *)s2); }
inline char* mbsspnp(const char * s1, const char * s2) { return (char*)_mbsspnp((const unsigned char *)s1, (const unsigned char *)s2); }
inline char* mbsstr(const char * s1, const char * s2) { return (char*)_mbsstr((const unsigned char *)s1, (const unsigned char *)s2); }
inline char* mbstok(char * s1, const char * s2) { return (char*)_mbstok((unsigned char *)s1, (const unsigned char *)s2); }
inline char* mbsupr(char * s) { return (char*)_mbsupr((unsigned char *)s); }
inline size_t mbclen(const char * s) { return _mbclen((const unsigned char *)s); }
inline void mbccpy(char * s1, const char *s2) { _mbccpy((unsigned char *)s1, (const unsigned char *)s2); }
inline int mbccmp(const char* s1, const char* s2) { return _mbccmp((const unsigned char*)s1, (const unsigned char*)s2); }


#endif //_MBCS_H_INCLUDED