#include <stdafx.h>
#include "dbgtrace.h"

void dbg_trace(const char* s)
{
	OutputDebugString(s);
	return;
}

void dbg_tracef(const char* format, ...)
{
	va_list v;
	va_start(v,format);
	dbg_tracefv(format, v);
	va_end(v);
	return;
}

void dbg_tracefv(const char* format, va_list v)
{
	char buf[512]={0};
	int c;
	
	c = vsprintf_s(buf, sizeof(buf)-1, format, v);
	if(c < 0)
		buf[sizeof(buf)-1] = 0;
		
	dbg_trace(buf);

	return;
}

void dbg_traces(const char* file, int line, const char* s)
{
	dbg_tracesf(file, line, s);  
	return;
}

void dbg_tracesf(const char* file, int line, const char* format, ...)
{
	va_list v;
	va_start(v,format);
	dbg_tracesfv(file, line, format, v);  
    va_end(v);
	return;
}

void dbg_tracesfv(const char* file, int line, const char* format, va_list v)
{
	char buf[512]={0};
	int c;
	
	c = sprintf_s(buf,sizeof(buf)-1,"%s(%d) : ", get_short_file_name(file), line);
	if(c > 0)
	{
		if( vsprintf_s(buf + c, sizeof(buf)-c-1, format, v) < 0 )
			buf[sizeof(buf)-c-1] = 0;
	}else{
		buf[sizeof(buf)-1] = 0;
	}
	
	dbg_trace(buf);
}

const char* get_short_file_name(const char* file)
{
	const char* pch;
	if(!file) return "";
	
	pch = strrchr(file, '\\');	
	if(!pch) pch = strrchr(file, ':');

	if( pch++ )	// if found, increment
	{
		if(*pch == 0)  // '\' is last symbol
		    pch = 0;
	}else{
		pch = file;
	}
	return pch;
}
