#include "stdafx.h"
#include "string.h"

namespace igo
{

inline char* mbsdec(const char * s1, const char * s2) { return (char*)_mbsdec((const unsigned char *)s1, (const unsigned char *)s2); }

char* string::null_str="";

string::string()
{ 
	data=null_str; 
}

string::string(int len)
{ 
	data=null_str; 
	setlen(len);
}

string::string(const string& s)
{ 
	data=null_str;	
	operator=(s); 
}

string::string(const char* s)
{ 
	data=null_str; 
	operator=(s); 
}

void string::free()
{ 
	if(data != null_str) 
		delete [] data;
	data = null_str;
}

char* string::forget()
{ 
	char* s=data; 
	data = null_str; 
	return s; 
}

void string::steal(string& str)
{
	free();
	data = str.forget();
}

int operator==(const string& str, const char* s)		
{ 
	if(str.data==s) return 1; 
	if(s==0) return	0;
	return !strcmp(str.data, s);
}

string& string::operator=(const string& s)
{ 
	return operator=((const char*)s); 
}

string& string::operator=(const char* s)
{
	string::free();
	if( s && *s )	// do not copy empty string
	{
		int len = strlen(s) + 1;
		data = new char [len];
		if(data) 
			memcpy(data, s, len);
		else
			data=null_str;
	}
	return *this;
}

string& string::operator+=(const char* s)
{ 
	if( s && *s ) // do not copy empty string
	{
		int l1 = strlen(data);
		int l2 = strlen(s);
		char* p = new char [l1 + l2 + 1];
		if(p)
		{
			memcpy(p, data, l1);
			string::free();
			memcpy(p + l1, s, l2 + 1);
			data = p;
		}
	}
	return *this;
}

char* string::setlen(int len)
{
	string::free();
	if(len>0)
	{
		data = new char [len + 1];
		if(data) 
			data[0]=0; // terminate string with zero
		else{
			data=null_str;
	        return 0;
        }
    }
	return data;
}



//int string::write(memfile* m)
//{
//	return m->write(data, strlen(data) + 1);
//}
//
//int string::read(memfile* m)
//{
//	char* buf = m->getbuf();
//	if(!buf) return 0;
//
//	int pos = m->getpos();
//	char* p = buf + pos;
//	int c = 0;
//	int len = strlen(p);
//	if(len)
//	{
//		setlen(len);
//		c = m->read(data, len + 1);
//		if(c != len + 1)
//			return -1;
//	}else{
//	   m->setpos(pos+1);
//	}
//	return c;
//}

int string::read(void* key, const char* value)
{
	DWORD dwType = REG_SZ;
	DWORD cbSize = 1024;
	char buf[1024+1];
	
	int ret = RegQueryValueEx((HKEY)key, value, 0, &dwType, (unsigned char*)buf, &cbSize);
	if(ret !=  ERROR_SUCCESS)
	{
		return -1;
	}
	
	buf[cbSize]=0;
	operator=((char*)buf);
	return 0;
}

int string::write(void* key, const char* value)
{
	int ret = RegSetValueEx((HKEY)key, value, 0, REG_SZ, (unsigned char*)data, strlen(data)+1);
	if(ret !=  ERROR_SUCCESS)
	{
		return -1;
	}
	return 0;
}

const char* string::load(void* hmodule, unsigned int id, const char* def_str)
{
	char buf[256]="";
	int l = LoadString((HINSTANCE)hmodule, id, buf, sizeof(buf)-1);
	if(l>0)
		def_str=buf;		

    operator=(def_str); 
	return data;
}



int string::printf(const char* format, ...)
{
	va_list v;
	va_start(v,format);
	int ret = vprintf(format, v);
	va_end(v);
	return ret;
}

extern int sformat_len(const char* format, va_list argList);

int string::vprintf(const char* format, va_list v)
{
	int len = sformat_len(format, v);
	if(!len) return -1;

	setlen(len);
	if ( data == null_str ) return -1;

	return vsprintf_s(data, len, format, v);
}

int string::rprintf(void *hmodule, int format, ...)
{
	string sformat;
	if ( sformat.load(hmodule,format) != null_str )
	{
		va_list v;
		va_start(v,format);
		int ret = vprintf(sformat, v);
		va_end(v);
		return ret;
	}
	else
	{
		return -1;
	}
}

int string::rvprintf(void *hmodule, int format, va_list v)
{
	string sformat;
	if ( sformat.load(hmodule,format) != null_str )
	{
		return vprintf(sformat, v);
	}
	else
	{
		return -1;
	}
}


int	string::format(const char* format, ...)
{
	va_list v;
	va_start(v,format);
	int ret = vformat(format,v);
	va_end(v);
	return ret;
}


int	string::vformat(const char* format, va_list v)
{
	LPVOID buf;
	int ret = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER|FORMAT_MESSAGE_FROM_STRING,format,0,0,(LPSTR)&buf,0,&v);
	if ( ret > 0 )
	{
		setlen(ret);
		strcpy_s(data,ret, (char*)buf);
		LocalFree(buf);
	}
	else
	{
		setlen(0);
	}

	return ret;
}

int string::rformat(void *hmodule, int format, ...)
{
	string sformat;
	sformat.load(hmodule,format);
	va_list v;
	va_start(v,format);
	int ret = vformat(sformat,v);
	va_end(v);
	return ret;

}

int string::rvformat(void *hmodule, int format, va_list v)
{
	string sformat;
	sformat.load(hmodule,format);
	return vformat(sformat,v);
}


int string::format_error(int dwErrorCode, bool bWithNumericCode)
{
	char* buf;
	int ret = FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER|FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,dwErrorCode,MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&buf,0,NULL);

	if ( ret > 0 )
	{
		int len = strlen(buf);

		while ( 1 )
		{
			char * pch = mbsdec(buf,buf+len);
			if ( *pch == '\n' || *pch == '\r' )
			{
				*pch = 0;
				--len;
			}
			else
			{
				break;
			}
		}

		if ( bWithNumericCode )
		{
			printf("%s 0x%08X(%d)",buf,dwErrorCode,dwErrorCode);
		}
		else
		{
			setlen(len);
			strcpy_s(data, len, buf);
		}

		LocalFree(buf);
	}
	else
	{
		if ( bWithNumericCode )
		{
			printf("0x%08X(%d)",dwErrorCode,dwErrorCode);
		}
		else
		{
			setlen(0);
		}
	}

	return ret;
}

//-----------------------------------------------------------------------
int unicode2ansi(const unsigned short* us, int usize, char* as, int asize)
{
	char* s = as;
	while(*us && usize && asize)
		*s++ = ((*us) & 0xFF), us++, usize -= 2, asize--;

	if(s - as < asize)
		*s=0;
	else
		s[asize-1]=0;

	return (s - as);
}

//-----------------------------------------------------------------------
int ansi2unicode(const char* as, int asize, unsigned short* us, int usize)
{
	unsigned short* s = us;
	while(*as && usize && asize)
		*s++ = *as++, usize -= 2, asize--;

	if(s - us < usize)
		*s=0;
	else
		s[usize-2]=0;

	return (s - us);
}

//-----------------------------------------------------------------------
const char* string::tolower()
{
	char* p = data;
    while(*p)
        *p++ = ::tolower(*p);

    return data;
}

//-----------------------------------------------------------------------
const char* string::toupper()
{
	char* p = data;
    while(*p)
        *p++ = ::toupper(*p);

    return data;
}

//-----------------------------------------------------------------------
#ifndef min
#define min(x,y)	(((x)<(y)) ? (x) : (y))
#define max(x,y)	(((x)>(y)) ? (x) : (y))
#endif

int sformat_len(const char* lpszFormat, va_list argList)
{
	va_list argListSave = argList;

	// make a guess at the maximum length of the resulting string
	int nMaxLen = 0;
	for (const char* lpsz = lpszFormat; *lpsz != '\0'; lpsz++)
	{
		// handle '%' character, but watch out for '%%'
		if (*lpsz != '%' || *(++lpsz) == '%')
		{
			nMaxLen += 1;
			continue;
		}

		int nItemLen = 0;

		// handle '%' character with format
		int nWidth = 0;
		for (; *lpsz != '\0'; lpsz++)
		{
			// check for valid flags
			if (*lpsz == '#')
				nMaxLen += 2;   // for '0x'
			else if (*lpsz == '*')
				nWidth = va_arg(argList, int);
			else if (*lpsz == '-' || *lpsz == '+' || *lpsz == '0' ||
				*lpsz == ' ')
				;
			else // hit non-flag character
				break;
		}
		// get width and skip it
		if (nWidth == 0)
		{
			// width indicated by
			nWidth = atoi(lpsz);
			for (; *lpsz != '\0' && isdigit(*lpsz); lpsz++)
				;
		}
		if(!(nWidth >= 0)) return 0;

		int nPrecision = 0;
		if (*lpsz == '.')
		{
			// skip past '.' separator (width.precision)
			lpsz++;

			// get precision and skip it
			if (*lpsz == '*')
			{
				nPrecision = va_arg(argList, int);
				lpsz++;
			}
			else
			{
				nPrecision = atoi(lpsz);
				for (; *lpsz != '\0' && isdigit(*lpsz); lpsz++)
					;
			}
			if(!(nPrecision >= 0)) return 0;
		}

		// should be on type modifier or specifier
		switch (*lpsz)
		{
		// modifiers that affect size
		case 'h':
		case 'l':
		// modifiers that do not affect size
		case 'F':
		case 'N':
		case 'L':
			lpsz++;
			break;
		}

		// now should be on specifier
		switch (*lpsz)
		{
		// single characters
		case 'c':
		case 'C':
			nItemLen = 2;
			va_arg(argList, char);
			break;

		// strings
		case 's':
		case 'S':
			{
				const char* pstrNextArg = va_arg(argList, const char*);
				if (pstrNextArg == 0)
					 nItemLen = 6;  // "(null)"
				else
				{
					 nItemLen = strlen(pstrNextArg);
					 nItemLen = max(1, nItemLen);
				}
				break;
			}
		}

		// adjust nItemLen for strings
		if (nItemLen != 0)
		{
			nItemLen = max(nItemLen, nWidth);
			if (nPrecision != 0)
				nItemLen = min(nItemLen, nPrecision);
		}
		else
		{
			switch (*lpsz)
			{
			// integers
			case 'd':
			case 'i':
			case 'u':
			case 'x':
			case 'X':
			case 'o':
				va_arg(argList, int);
				nItemLen = 10;
				nItemLen = max(nItemLen, nWidth+nPrecision);
				break;

			case 'e':
			case 'f':
			case 'g':
			case 'G':
				va_arg(argList, double);
				nItemLen = 64;
				nItemLen = max(nItemLen, nWidth+nPrecision);
				break;

			case 'p':
				va_arg(argList, void*);
				nItemLen = 10;
				nItemLen = max(nItemLen, nWidth+nPrecision);
				break;

			// no output
			case 'n':
				va_arg(argList, int*);
				break;

			default:
				return 0;  // unknown formatting option
			}
		}

		// adjust nMaxLen for output nItemLen
		nMaxLen += nItemLen;
	}

	va_end(argListSave);
	return nMaxLen;
}

} // namespace igo