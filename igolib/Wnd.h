#pragma once
/********************************************
*
*  class Wnd  (generic base class for all Win32 windows)
*  class WndClass<T> (singletone to register Win32 window class)
*
*  definition
*
********************************************/
namespace igo
{

#define WC_IGOWND             "igownd_"
#define WND_ABRA_CADABRA	  "abracadabra"
#define WND_ABRA_CADABRA_DLG  "abracadabradlg"

class Wnd;

// Message Map Macroses
#define MSG_MAP_DEF() \
	public:	\
	static const char* szClassName; \
	virtual const char* GetClassName(); \
	protected: \
	static MSG_MAP_ITEM msgmap[];	\
	static unsigned mapsize; \
	virtual unsigned* GetMsgMap(); \
	virtual WNDPROC GetWndProcAdr(); \
	LRESULT SuperWndProc(UINT msg, WPARAM w, LPARAM l);


#define MSG_MAP_BEG(theClass, baseClass) \
	LRESULT theClass::SuperWndProc(UINT msg, WPARAM w, LPARAM l) \
	{ \
		return WndProcMsgMap(msg,w,l,(unsigned*)baseClass::msgmap); \
	} \
	const char* theClass::szClassName = "igownd_"#theClass; \
	const char* theClass::GetClassName() \
		{ return theClass::szClassName; } \
	unsigned* theClass::GetMsgMap() \
		{ return (unsigned*)theClass::msgmap; } \
	WNDPROC theClass::GetWndProcAdr(){ return WndClass<theClass>::WndProc; } \
	MSG_MAP_ITEM theClass::msgmap[] = { \
	(unsigned)baseClass::msgmap, 0, \

#define MSG_MAP_BEG_EX(theClass, baseClass, szCustomClassName) \
	LRESULT theClass::SuperWndProc(UINT msg, WPARAM w, LPARAM l) \
	{ \
		return WndProcMsgMap(msg,w,l,(unsigned*)baseClass::msgmap); \
	} \
	const char* theClass::szClassName = "igownd_"#szCustomClassName; \
	const char* theClass::GetClassName() \
		{ return theClass::szClassName; } \
	unsigned* theClass::GetMsgMap() \
		{ return (unsigned*)theClass::msgmap; } \
	WNDPROC theClass::GetWndProcAdr(){ return WndClass<theClass>::WndProc; } \
	MSG_MAP_ITEM theClass::msgmap[] = { \
	(unsigned)baseClass::msgmap, 0, \

#define ON_MSG(msg, memb) \
	(msg), (PFN_WNDPROC)&(memb),

#define MSG_MAP_END(theClass) \
	}; \
	unsigned theClass::mapsize = \
	(((unsigned*)theClass::msgmap)[1] = sizeof(theClass::msgmap)/sizeof(MSG_MAP_ITEM) - 1);

//---------------------------------------------
// Wnd
//---------------------------------------------
struct MSG_MAP_ITEM;

class Wnd
{
public:
	int		m_bFromCreate;
	HWND	m_hWnd;

	Wnd();
	virtual ~Wnd();
	int IsWndClass();

	HWND Create(DWORD dwExStyle, DWORD dwStyle,
			LPCSTR szClass, LPCSTR szCaption,
			int x, int y, int dx, int dy,
			HWND hParent, UINT nID, HINSTANCE hInst=0);

	HWND Create(DWORD dwExStyle, DWORD dwStyle,
			LPCSTR szClass, LPCSTR szCaption,
			const RECT& rc, HWND hParent, UINT nID, HINSTANCE hInst=0)
			{
				return Create(dwExStyle, dwStyle, szClass, szCaption,
							rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top,
							hParent, nID, hInst);
			}


	static Wnd* FromHandle(HWND hwnd);
	virtual LRESULT WndProc(UINT msg, WPARAM wParam, LPARAM lParam);
	virtual LRESULT DefWndProc(UINT msg, WPARAM wParam, LPARAM lParam);
	virtual const char* GetClassName() = 0;

	int Subclass(HWND hwnd);
	int UnSubclass();

protected:
	virtual LRESULT WndProcMsgMap(UINT msg, WPARAM wParam, LPARAM lParam, unsigned* pMsgMap);

	WNDPROC m_fnSuclassedWndProc;

	static MSG_MAP_ITEM* msgmap;
	virtual unsigned* GetMsgMap(){ return 0; }
	virtual WNDPROC GetWndProcAdr(){ return 0; }

private:
	static char* szClassName;
};

typedef LRESULT (Wnd::*  PFN_WNDPROC)(WPARAM, LPARAM);

#pragma pack(push, 1)
struct MSG_MAP_ITEM
{
	unsigned msg;
	PFN_WNDPROC pfn;
};
#pragma pack(pop)

struct WND_THIS_STRUCT
{
	const char* s;
	void* pThis;
};


//---------------------------------------------
// WndClass
//---------------------------------------------
template<class T>
class WndClass
{
friend class Wnd;
public:

	WndClass()
	{
		Register(0);
	}

	WndClass(HINSTANCE hInst)
	{
		Register(hInst);
	}

	WndClass(HINSTANCE hInst, HICON hIcon, HBRUSH hBrush, PCSTR pszMenu)
	{
		Register(hInst, hIcon, hBrush, pszMenu);
	}

	WndClass(WNDCLASS& wc)
	{
		Register(wc);
	}

	static LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		LRESULT res = 0;
		T*	pThis = NULL;

		switch(msg)
		{
			case WM_INITDIALOG:
				{
					WND_THIS_STRUCT* pts = (WND_THIS_STRUCT*)lParam;

					if(pts && strcmp(pts->s, WND_ABRA_CADABRA_DLG)==0 )
						pThis = (T*)pts->pThis;

					if( pThis )
					{
						pThis->m_bFromCreate = TRUE;

					}else{
						pThis = new T;
					}

					if( !pThis )
						return 0;

					pThis->m_hWnd = hWnd;
					SetWindowLong(hWnd, GWL_USERDATA, (long)pThis);
					res = pThis->WndProc(msg,wParam,lParam);

				}
				break;

			case WM_NCCREATE:
				{
					CREATESTRUCT* pcs = (CREATESTRUCT*)lParam;
					WND_THIS_STRUCT* pts = (WND_THIS_STRUCT*)pcs->lpCreateParams;

					if(pts && strcmp(pts->s, WND_ABRA_CADABRA)==0 )
						pThis = (T*)pts->pThis;

					if( pThis )
					{
						if(pThis->m_hWnd != 0) // already attached
							return 0;
						pThis->m_bFromCreate = TRUE;

					}else{
						pThis = new T;
					}

					if( !pThis )
						return 0;

					pThis->m_hWnd = hWnd;
					SetWindowLong(hWnd, GWL_USERDATA, (long)pThis);
					res = pThis->WndProc(msg,wParam,lParam);

				}
				break;

			case WM_NCDESTROY:
				if( (pThis = (T*)T::FromHandle(hWnd)) )
				{
					res = pThis->WndProc(msg, wParam, lParam);
					pThis->m_hWnd = 0;
					if( !pThis->m_bFromCreate )
						delete pThis;
				}
				break;

			default:
				if( (pThis = (T*)T::FromHandle(hWnd)) )
				{
					return pThis->WndProc(msg, wParam, lParam);
				}
		}
		return res;
	}

protected:

	class DlgConstructorType { void* unused; };

	WndClass(DlgConstructorType fake_param)
	{
	}

	static int Register(HINSTANCE hInst)
	{
		return Register(hInst, 0, (HBRUSH)(COLOR_WINDOW+1), 0);
	}

	static int Register(HINSTANCE hInst, HICON hIcon, HBRUSH hBrush, PCSTR pszMenu)
	{
		WNDCLASS wc;

		// Check is allready registered
		if( GetClassInfo(hInst, T::szClassName, &wc) )
			return TRUE;

		if(hInst == 0)
			hInst = GetModuleHandle(0);

		wc.style         = CS_DBLCLKS;
		wc.cbClsExtra    = 0;
		wc.cbWndExtra    = 0;
		wc.hInstance     = hInst;
		wc.hIcon         = hIcon;
		wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = hBrush;
		wc.lpszMenuName  = pszMenu;

		return Register(wc);
	}

	static int Register(WNDCLASS& wc)
	{
		if(wc.hInstance==0)
			wc.hInstance = GetModuleHandle(0);

		wc.lpfnWndProc   = WndClass::WndProc;
		wc.lpszClassName = T::szClassName;

		return RegisterClass(&wc);
	}

};

}