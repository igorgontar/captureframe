#include "stdafx.h"
#include "thread.h"
#include "dbgtrace.h"

namespace igo
{

Thread::Thread()
{
	Reset();
	m_bAutoDelete = 0;
}

Thread::Thread(IRunnable* p, void* param)
{
	Reset();
	m_bAutoDelete = 0;
	if(p)
	{
		m_bAutoDelete = 1;
		Start(p, param);
	}
}

//------------------------------------------------------------
Thread::~Thread()
{
	Stop();
}

//------------------------------------------------------------
void Thread::Reset()
{
	m_pRunnable = 0;
    m_param     = 0;
	m_hThr      = 0;
	m_dwID		= 0;	
}

//------------------------------------------------------------
BOOL Thread::Start(IRunnable* p, void* param)
{
	if(IsRunning()) return 0;

	Stop();

	m_pRunnable = p;
    m_param = param;

	unsigned id=0;
	unsigned h = _beginthreadex(
		 0,							// No security attributes.
		 0,							// Use same stack size.
		 (unsigned (__stdcall *)(void *))stThreadProc,	// Thread procedure.
		 (void*)(this),				// Parameter to pass.
		 0,							// Run immediately.
		 &id);
	
	if(!h)
	{
		INF_TRACE1("error: _beginthreadex() : %d\n",_errno);
		return 0;
	}

	m_hThr = (HANDLE)h;
	m_dwID = id;
	return 1;
}

//------------------------------------------------------------
void Thread::Stop()
{ 
	if(IsRunning())
	{
		UnblockRun(); 
		WaitUntilRunExit(); 
		if(m_hThr) 
			CloseHandle(m_hThr); 
		Reset(); 
	}
}

//------------------------------------------------------------
BOOL Thread::getExitCode(DWORD* pdwExitCode)
{
	if(!m_hThr || !pdwExitCode || !GetExitCodeThread(m_hThr, pdwExitCode) ) 
		return 0;
	return 1;
}

//------------------------------------------------------------
BOOL Thread::IsRunning()
{
	DWORD dwExitCode=0;
	return ( getExitCode(&dwExitCode) && dwExitCode == STILL_ACTIVE);
}

//------------------------------------------------------------
void Thread::WaitUntilRunExit() 
{ 
	if(m_hThr) 
		WaitForSingleObject(m_hThr, INFINITE); 
}

//------------------------------------------------------------
int Thread::Run(void* param) 
{
	if(!m_pRunnable) return 0;
	return m_pRunnable->Run(param);		
}

//------------------------------------------------------------
void Thread::UnblockRun() 
{
	if(m_pRunnable)
		m_pRunnable->UnblockRun();		
}

//------------------------------------------------------------
void Thread::Release()
{
	if(this)
		delete this;
} 

//------------------------------------------------------------
void __stdcall Thread::stThreadProc(Thread* pThis)
{
	if(pThis)
	{
		int ret = pThis->Run(pThis->m_param);
		_endthreadex(ret);

		if(pThis->m_bAutoDelete)
			pThis->Release();
	
	}else{
		_endthreadex(-1);
	}
}

} // namespace igo


