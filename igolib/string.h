//
// Simplest string implementation
//
#ifndef __IGO_STRING_H
#define __IGO_STRING_H

namespace igo
{

class memfile;

struct str_base {	// use this struct (without constructors)
	char* data;				// as a base class to allow using it
							// as a member of union 
};

class string : protected str_base
{
	friend int operator==(const string& str, const char * pszStr);
public:
	string();										
	string(int len);										
	string(const string& s); 
	string(const char* s);			
	~string()	{ string::free(); }													

	string& operator=(const string& s);
	string& operator=(const char* s);
	int operator==(char ch)				{ return (*data==ch); }
	operator const char*() const		{ return (const char*)data; }
	operator char*() const				{ return data; }
	operator char() const				{ return *data; }

	char operator[] (int i) const { return data?data[i]:0; }
	char& operator[] (int i) { return data[i]; }

	string& operator+=(const char* s);
	string& operator+=(const string& s) { return operator+=((const char*)s); }

	int is_empty() const				{ return (data==0 || *data==0); }

	void free();

	void steal(string& str);

	char* forget();
	char* setlen(int len);	// remove the content and allocate memory for len chars + 1 byte for z-term.
	const char* load(void* hmodule, unsigned int id, const char* def_str=0);

	int write(memfile* m);
	int read(memfile* m);

	int read(void* hkey, const char* value);
	int write(void* hkey, const char* value);

	int printf(const char* format, ...); //uses printf-like format specifier
	int vprintf(const char* format, va_list v);

	int rprintf(void *hmodule, int format, ...);
	int rvprintf(void *hmodule, int format, va_list v);


	int	format(const char* format, ...);//uses FormatMessage-like format specifier
	int	vformat(const char* format, va_list v);

	int rformat(void *hmodule, int format, ...);
	int rvformat(void *hmodule, int format, va_list v);

	int format_error(int dwErrorCode, bool bWithNumericCode=true);

    const char* tolower(); // tolowers and return ptr to our string buffer
    const char* toupper(); // touppers and return ptr to our string buffer

	static char* null_str;
};

int operator==(const string& str, const char* s);		

inline int operator==(const char * pszStr,  const string& s)
{
	return s == pszStr;
}

inline int operator==(const string& s1,  const string& s2)
{
	return s1 == (const char*)s2;
}


inline int operator!=(const char * pszStr,  const string& s)
{
	return !(pszStr==s);
}

inline int operator!=(const string& str, const char * pszStr)
{
	return !(str==pszStr);
}


inline int operator!=(const string& s1,  const string& s2)
{
	return !(s1==s2);
}

int unicode2ansi(const unsigned short* us, int usize, char* as, int asize);
int ansi2unicode(const char* as, int asize, unsigned short* us, int usize);

} // namespace igo

#endif // __IGO_STRING_H

