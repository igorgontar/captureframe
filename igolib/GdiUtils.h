#pragma once

int InitGdiUtils();
void DeInitGdiUtils();
void DrawHatchRect1(HDC hdc, int x, int y, int cx, int cy);
void DrawHatchRect2(HDC hdc, int x, int y, int cx, int cy);
void FillSolidRect(HDC hdc, LPCRECT lpRect, COLORREF clr);
void FillSolidRect(HDC hdc, int x, int y, int dx, int dy, COLORREF clr);
void Draw3DRect(HDC hdc, int x, int y, int cx, int cy, COLORREF clrTopLeft, COLORREF clrBottomRight);
void Draw3DRect(HDC hdc, LPCRECT lpRect, COLORREF clrTopLeft, COLORREF clrBottomRight);
void DrawSplitBar(HDC hdc, RECT* prc);
