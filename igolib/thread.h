#ifndef __IGO_THREAD_H
#define __IGO_THREAD_H
namespace igo
{
//-----------------------------------------------------------------
//
//-----------------------------------------------------------------
struct IRunnable
{
	virtual int  Run(void* param)=0; 
	virtual void UnblockRun()=0; 
};

class Thread : protected IRunnable
{
public:
	Thread(IRunnable* p, void* param);	// in this case thread will delete itself (call Release) after Run() exit
							// be sure to not keep ptr to this object
							// usage:  new Thread(this); // where this implements IRunnable
	
	Thread();			    // in this case you should call Start(), Stop(), Close() explicitely
	virtual ~Thread();
	
	BOOL Start(IRunnable* p, void* param=0);
	void Stop();
	void WaitUntilRunExit();
	BOOL IsRunning();

	HANDLE getHandle()		{ return m_hThr; }
	DWORD  getID()			{ return m_dwID; }
	BOOL   getExitCode(DWORD* pdwExitCode);


	virtual void UnblockRun(); 

protected:
	IRunnable*  m_pRunnable;
    void*       m_param;
	
	virtual int  Run(void* param); 
	virtual void Release();  // default implementation is: delete this;

private:
	HANDLE m_hThr;
	DWORD  m_dwID;
	BOOL   m_bAutoDelete;

	void Reset();
	static void __stdcall stThreadProc(Thread* pThis);
};

struct ILockable
{
	virtual void Lock() = 0;
	virtual void UnLock() = 0;
};

struct Locker
{
	ILockable* m_pl;
	
	Locker(ILockable* pl)
	{
		if(pl)
		{	m_pl = pl;
			m_pl->Lock();
		}
	}

	~Locker()
	{
		if(m_pl) m_pl->UnLock();
	}
};

class Lockable : public ILockable
{
public:

	Lockable()   {  InitializeCriticalSection(&m_critical_section); }
	~Lockable()  {  DeleteCriticalSection(&m_critical_section); }

	void Lock()		{ EnterCriticalSection(&m_critical_section); }
	void UnLock()	{ LeaveCriticalSection(&m_critical_section); }

protected:
	CRITICAL_SECTION m_critical_section;
};

template <class T>
class LockableImpl : public T
{
public:

	LockableImpl()   {  InitializeCriticalSection(&m_critical_section); }
	~LockableImpl()  {  DeleteCriticalSection(&m_critical_section); }

protected:
	void Lock()		{ EnterCriticalSection(&m_critical_section); }
	void UnLock()	{ LeaveCriticalSection(&m_critical_section); }

	CRITICAL_SECTION m_critical_section;
};

}// namespace igo

#endif //__IGO_THREAD_H

