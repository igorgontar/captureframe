#include "StdAfx.h"
#include "Main.h"
#include "EdgeFrameWnd.h"

#define DX_DEFAULT 640
#define DY_DEFAULT 480
#define DY_TITLE	18 // including DY_BORDER 
#define DY_BORDER	 6 
#define DX_BORDER	 6 
#define DM_SIZER	32 
#define DM_BTN  	10 
#define COL_FRAME   RGB(253,106,46)
#define COL_SIZER   RGB(255,100,0)
#define COL_TEXT    RGB(0,0,0)

static char* s_szTitle = "";
static char* s_szSmallTitle = "egde presenter";

//--------------------------------------------------------------------
static WndClass<EdgeFrameWnd> wnd_class(0,0,0,0);

MSG_MAP_BEG(EdgeFrameWnd, Wnd)
	ON_MSG(WM_CREATE,		OnCreate	) 
	ON_MSG(WM_DESTROY,		OnDestroy	)
	ON_MSG(WM_SIZE,			OnSize		)
	ON_MSG(WM_MOVE,			OnMove		)
	ON_MSG(WM_NCHITTEST,	OnNcHitTest	)
	ON_MSG(WM_PAINT,		OnPaint		)
	ON_MSG(WM_ERASEBKGND,	OnEraseBkgnd)
MSG_MAP_END(EdgeFrameWnd)

//--------------------------------------------------------------------
EdgeFrameWnd::EdgeFrameWnd(void) : m_opacity(0)
{
}

//--------------------------------------------------------------------
EdgeFrameWnd::~EdgeFrameWnd(void)
{
}

//--------------------------------------------------------------------
void EdgeFrameWnd::Show()
{
	ShowWindow(m_hWnd, SW_SHOW);
	m_tranWnd.Show();
}

//--------------------------------------------------------------------
void EdgeFrameWnd::Hide()
{
	m_tranWnd.Hide();
	ShowWindow(m_hWnd, SW_HIDE);
}

//--------------------------------------------------------------------
BOOL EdgeFrameWnd::Create(HWND hOwner, int opacity) 
{ 
	m_opacity = opacity;
	HWND h = super::Create(
		0
		|WS_EX_TOOLWINDOW
		,
		0
		|WS_POPUP
		,
		0,	// class name, 0: use default name
		s_szTitle,	// caption text
		0, 0, 32, 32,	// window size is irrelevant until it gets attached to the Edge window
		hOwner,	// parent is owner in this case and it is Edge window it is atached 
		(UINT)0, // ID or hMenu
		0	     // hInstance 0: use default
	);
	if(!h) 
        return 0;		 	
	return 1;
}

//--------------------------------------------------------------------
LRESULT EdgeFrameWnd::OnCreate(WPARAM w, LPARAM l)
{
	if(!m_tranWnd.Create(m_hWnd,0,0,32,32,m_opacity))
		return -1;

	return 0; // return -1; // on error
}

//--------------------------------------------------------------------
LRESULT EdgeFrameWnd::OnDestroy(WPARAM w, LPARAM l)
{
	return 0;
}

//--------------------------------------------------------------------
void EdgeFrameWnd::SetInnerRect(const RECT& rc)
{
	RECT outer = rc;
	outer.left	 -= DX_BORDER;
	outer.right	 += DX_BORDER;
	outer.top	 -= DY_TITLE;
	outer.bottom += DY_BORDER;

	MoveWindow(m_hWnd, outer.left, outer.top, outer.right-outer.left, outer.bottom-outer.top, 1);
}

//--------------------------------------------------------------------
BOOL EdgeFrameWnd::GetInnerRect(RECT* prc)
{
	if(!prc)
		return 0;

	RECT rc = {0,0,0,0};
	if(GetWindowRect(m_hWnd, &rc))
	{
		rc.left		+= DX_BORDER;
		rc.right	-= DX_BORDER;
		rc.top		+= DY_TITLE;
		rc.bottom	-= DY_BORDER;

		*prc = rc;
	}
	return 1;
}

//--------------------------------------------------------------------
LRESULT EdgeFrameWnd::OnSize(WPARAM w, LPARAM l)
{
	RECT rc = {0,0,0,0};
	GetClientRect(m_hWnd, &rc);
	InvalidateRect(m_hWnd, &rc, false);

	int dx = rc.right - rc.left;
	int dy = rc.bottom - rc.top;
	if(dx <= 0) dx = 32;
	if(dy <= 0) dy = 16;

	RECT outer = {0,0,dx,dy};

	RECT inner = outer;
	inner.left += DX_BORDER;
	inner.top += DY_TITLE;
	inner.right -= DX_BORDER;
	inner.bottom -= DY_BORDER;

	HRGN hrgn1 = CreateRectRgnIndirect(&outer);
	HRGN hrgn2 = CreateRectRgnIndirect(&inner);
	HRGN hrgn = CreateRectRgn(0,0,1,1);

	CombineRgn(hrgn,hrgn1,hrgn2,RGN_DIFF);
	
	DeleteObject(hrgn1);
	DeleteObject(hrgn2);

	SetWindowRgn(m_hWnd, hrgn, 1);

	AdjustTranWnd();
	return 0;
}

//--------------------------------------------------------------------
LRESULT EdgeFrameWnd::OnMove(WPARAM w, LPARAM l)
{
	AdjustTranWnd();
	return 0;
}

//--------------------------------------------------------------------
void EdgeFrameWnd::AdjustTranWnd()
{
	RECT rc;
	GetInnerRect(&rc); // this is important
	//GetWindowRect(m_hWnd, &rc);
	MoveWindow(m_tranWnd.m_hWnd, rc.left, rc.top, rc.right-rc.left, rc.bottom-rc.top, 1); 
}

//--------------------------------------------------------------------
LRESULT EdgeFrameWnd::OnNcHitTest(WPARAM wParam, LPARAM lParam)
{
	return HTCLIENT;
}

//--------------------------------------------------------------------
LRESULT EdgeFrameWnd::OnEraseBkgnd(WPARAM w, LPARAM l)
{
	return 1;
}

//--------------------------------------------------------------------
LRESULT EdgeFrameWnd::OnPaint(WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	RECT rc;
	GetClientRect(m_hWnd, &rc);
	HDC hdc = BeginPaint(m_hWnd, &ps);

	FillSolidRect(hdc, &rc, COL_FRAME);
	
	SetTextColor(hdc, COL_TEXT);
	SelectObject(hdc, GetStockObject(ANSI_VAR_FONT));
	TextOut(hdc, 4, 2, s_szSmallTitle, strlen(s_szSmallTitle));

	EndPaint(m_hWnd, &ps);
	return 0;
}



