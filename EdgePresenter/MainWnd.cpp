#include "StdAfx.h"
#include "Main.h"
#include "MainWnd.h"
#include "Monitor.h"

#define WM_GIU_THREAD_INVOKE  WM_USER+100

static char* s_szTitle = "Edge Presenter";

//--------------------------------------------------------------------
static WndClass<MainWnd> wnd_class(0,0,0,0);

MSG_MAP_BEG(MainWnd, Wnd)
	ON_MSG(WM_CREATE,		OnCreate	) 
	ON_MSG(WM_DESTROY,		OnDestroy	)
	ON_MSG(WM_ERASEBKGND,	OnEraseBkgnd)
	ON_MSG(WM_GIU_THREAD_INVOKE, OnGuiThreadInvoke)
MSG_MAP_END(MainWnd)

//--------------------------------------------------------------------
MainWnd::MainWnd(void) : m_pMonitor(0)
{
}

//--------------------------------------------------------------------
MainWnd::~MainWnd(void)
{
}

//--------------------------------------------------------------------
BOOL MainWnd::Create() 
{ 
	// NOTE: (dx,dy)=0 makes this window physically invisible, 
	//       while the system thinks it is and shows the app in taskbar
	HWND h = super::Create(
		0, // ex style
		WS_POPUP|WS_VISIBLE,
		0, // class name, 0: use default name
		s_szTitle,	
		0, // x
		0, // y
		0,// dx 
		0,// dy
		0, // parent
		0, // ID or hMenu
		0  // hInstance 0: use default
	);
	if(!h) 
        return 0;		 	
	return 1;
}

//--------------------------------------------------------------------
LRESULT MainWnd::OnCreate(WPARAM w, LPARAM l)
{
	SetClassLong(m_hWnd, GCL_HICON, (long)LoadIcon(GetResHandle(), MAKEINTRESOURCE(IDI_MAIN_ICON)));
	return 0; // on error return -1;
}

//--------------------------------------------------------------------
LRESULT MainWnd::OnDestroy(WPARAM w, LPARAM l)
{
	PostQuitMessage(0);
	return 0;
}

//--------------------------------------------------------------------
LRESULT MainWnd::OnEraseBkgnd(WPARAM w, LPARAM l)
{
	return 1;
}

//--------------------------------------------------------------------
BOOL MainWnd::BeginInvoke(unsigned cmd, void* param)
{
	BOOL ret = PostMessage(m_hWnd, WM_GIU_THREAD_INVOKE, cmd, (LPARAM)param);
	return ret;
}

//--------------------------------------------------------------------
LRESULT MainWnd::OnGuiThreadInvoke(WPARAM w, LPARAM l)
{
	// this is executed on GUI thread, i.e. thread on which this window has been created
	// and which is running mesage loop	for this window
	if(m_pMonitor)
		m_pMonitor->OnGuiThreadInvoke(w, (void*)l);
	return 0;
}


