#include "stdafx.h"
#include "Main.h"
#include "MainWnd.h"
#include "Monitor.h"

static HINSTANCE g_hInst = 0;
HINSTANCE GetResHandle() { return g_hInst; }; 

int APIENTRY _tWinMain(HINSTANCE hInst,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	g_hInst = hInst;

	InitGdiUtils();

	MainWnd wnd;
	if(!wnd.Create())
		return -1;

	Monitor monitor(wnd);
	wnd.LinkMonitor(&monitor);

	monitor.Start(); 

	MSG msg;
	HACCEL hAccelTable = LoadAccelerators(hInst, MAKEINTRESOURCE(IDC_MAIN));
	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	// I don't care about grasefull shutdown, it's not needed
	// if the user closed main window I just forse process to exit instead of grasefully closing all bkground threads
	ExitProcess(0);
	return 0;
}

