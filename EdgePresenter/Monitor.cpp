#include "StdAfx.h"
#include "Monitor.h"

#define EDGE

#ifdef _DEBUG
# define SLEEP_INTERVAL   100
# define SCAN_INTERVAL    3000  // must be >= SLEEP_INTERVAL
# define UPDATE_INTERVAL  200   // 0 - update on each sleep cycle
#else
# define SLEEP_INTERVAL   10
# define SCAN_INTERVAL    1000  // must be >= SLEEP_INTERVAL
# define UPDATE_INTERVAL  0	   // 0 - update on each sleep cycle
#endif

#ifdef EDGE
# define EDGE_OWNER_CLASS     "ApplicationFrameWindow"
# define EDGE_OWNER_CAPTION   "Microsoft Edge"

# define EDGE_CAPTURE_CLASS    "Windows.UI.Core.CoreWindow"
# define EDGE_CAPTURE_CAPTION  "Microsoft Edge"
#else
# define EDGE_OWNER_CLASS     "IEFrame"
# define EDGE_OWNER_CAPTION   "Internet Explorer"

# define EDGE_CAPTURE_CLASS    "Frame Tab"
# define EDGE_CAPTURE_CAPTION  ""
#endif


//--------------------------------------------------------------------
Monitor::Monitor(MainWnd& wnd) : m_invokeWnd(wnd), m_helpShown(0)
{
}

//--------------------------------------------------------------------
Monitor::~Monitor(void)
{
}

//--------------------------------------------------------------------
void Monitor::Start()
{
	m_thread.Start(this);
}

//--------------------------------------------------------------------
int Monitor::Run(void* param)
{
	unsigned t1 = GetTickCount();
	unsigned t2 = t1;

	for(int i=0;;i++)
	{
		Sleep(SLEEP_INTERVAL);
		unsigned now = GetTickCount();
		
		unsigned delta1 = now - t1;
		if(delta1 >= UPDATE_INTERVAL)
		{
			t1 = now;
			UpdatePositions();	
		}
		
		unsigned delta2 = now - t2;
		if(delta2 >= SCAN_INTERVAL)
		{
			t2 = now;
			ScanForNew();	
		}
	}
}

//--------------------------------------------------------------------
BOOL CALLBACK Monitor::stEnumTopWindowsProc(HWND hwnd, LPARAM lParam)
{
	 Monitor* pThis = (Monitor*)lParam;
	 if(pThis)
		 return pThis->EnumTopWindowsProc(hwnd);
	 return FALSE;
}

//--------------------------------------------------------------------
void Monitor::ScanForNew()
{
	DBG_TRACE("Scanning for new Edge windows...\n");
	// I use callback style for top level windows, because I want to exclude 
	// windows by style, before comparing strings, which is expensive
	//
	// On second step I use explicit enumeration with FindWindow, where perf. is irrelevant, because there will be only 
	// a few children and I only need the first one
	EnumWindows(stEnumTopWindowsProc, (LPARAM)this);

	if(!m_helpShown)
	{
		m_helpShown = 1;
		BeginInvoke(SHOW_HELP, 0);
	}
}

//--------------------------------------------------------------------
BOOL Monitor::EnumTopWindowsProc(HWND hTop)
{
	// NOTE: to avoid unnecessary allocations I make them static
	//       these variables will be only used by one scanning thread, so there is no concurrency issue
	//       I love that I can make static vars inside the function, missing this feature in .NET
	const int len = 512;
	static char class_name[len] = {0};
	static char caption[len] = {0};

	LONG dwStyle = GetWindowLong(hTop, GWL_STYLE);
	if( !(dwStyle & WS_CHILDWINDOW) && !(dwStyle & WS_CHILD))
	{
		class_name[0] = 0; 	// clear string without re-allocation
		::GetClassName(hTop, class_name, len);
		if(!strcmp(class_name, EDGE_OWNER_CLASS))
		{
			caption[0] = 0;
			::GetWindowText(hTop, caption, len);
			if(strstr(caption, EDGE_OWNER_CAPTION))
			{
				DBG_TRACE1("Found Edge top level window hwnd=0x%08X\n", hTop);
				// get first child
				HWND hChild = FindWindowEx(hTop, 0, EDGE_CAPTURE_CLASS, NULL);
				if(hChild)
				{
					caption[0] = 0;
					::GetWindowText(hChild, caption, len);
					if(strstr(caption, EDGE_CAPTURE_CAPTION))
					{
						DBG_TRACE1("Found Edge child window hwnd=0x%08X\n", hChild);
						Attach(hChild);
					}
				}
			}
		}
	}

	return 1;
}

//--------------------------------------------------------------------
void Monitor::Attach(HWND hCapture)
{
	Locker lock(&m_sync);

	// check if it already exist to avoid performing expencive Invoke() 
	MonitorItem* p = m_map.find(hCapture);
	if(p)
		return;

	BeginInvoke(ATTACH, hCapture);
}

//--------------------------------------------------------------------
void Monitor::Detach(HWND hCapture)
{
	Locker lock(&m_sync);
	// check if it is already removed to avoid performing expencive Invoke() 
	MonitorItem* p = m_map.find(hCapture);
	if(!p)
		return;
	BeginInvoke(DETACH, hCapture);
}

//--------------------------------------------------------------------
void Monitor::UpdatePositions()
{
	// perform cheap check first
	if(m_map.get_size() == 0)
		return;

	DBG_TRACE("Updating frame positions...\n");
	Locker lock(&m_sync);
	unsigned unused;
	void* pos = m_map.get_first_position();
	while(pos)
	{
		MonitorItem* p = m_map.get_next(pos, unused);
		if(!p->AdjustRect())
		{
			BeginInvoke(DETACH, p->GetCaptureWnd());
		}
	}
}

//--------------------------------------------------------------------
void Monitor::OnAttach(HWND hCapture)
{
	Locker lock(&m_sync);

	int exist = 0;
	MonitorItem* p = m_map.insert(hCapture, exist);
	if(exist)
		return;

	p->Create(hCapture);
}


//--------------------------------------------------------------------
void Monitor::OnDetach(HWND hCapture)
{
	Locker lock(&m_sync);
	m_map.remove(hCapture);
}


static const char* s_szHelpText = 
"Looks like there is no Microsoft Edge browser open at the moment\r\n"
"\r\n"
"Please, open at least one Edge browser window before starting application sharing via Skype (Lync)\r\n"	
"You will see an orange color frame around Edge browser with the caption: 'edge presenter'\r\n"	
"\r\n"
"After that you can start sharing Edge via Skype, by opening"
"'Present programs' menu and selecting 'Edge Presenter' from the list of available programs\r\n"
"\r\n"
"Press OK to close this 'Help' window, otherwise it will be presented too.\r\n"
"\r\n"
"Please note, the 'Edge Presenter' icon on the Taskbar should remain running\r\n"
"";

//--------------------------------------------------------------------
void Monitor::OnShowHelp()
{
	DBG_TRACE("OnShowHelp()\n");
	int count = 0;
	{
		Locker lock(&m_sync);
		count = m_map.get_size();
	}
	if(count == 0)
	{
		MessageBox(0, s_szHelpText, "Edge Presenter Help", MB_OK|MB_ICONINFORMATION);
	}
}

//--------------------------------------------------------------------
void Monitor::OnGuiThreadInvoke(unsigned cmd, void* param)
{
	switch(cmd)
	{
	case ATTACH:
		OnAttach((HWND)param);
		break;
	case DETACH:
		OnDetach((HWND)param);
		break;
	case SHOW_HELP:
		OnShowHelp();
		break;
	}
}
