#pragma once
using namespace igo;

class Monitor;

class MainWnd : public Wnd
{
public:
	MainWnd(void);
	virtual ~MainWnd(void);

	BOOL Create(); 
	BOOL BeginInvoke(unsigned cmd, void* param);
	void LinkMonitor(Monitor* pMonitor) { m_pMonitor = pMonitor; }

protected:
	typedef Wnd super;
	Monitor* m_pMonitor;

	// message handlers
	LRESULT OnCreate(WPARAM w, LPARAM l);
	LRESULT OnDestroy(WPARAM w, LPARAM l);
	LRESULT OnGuiThreadInvoke(WPARAM w, LPARAM l);
	LRESULT OnEraseBkgnd(WPARAM w, LPARAM l);

	MSG_MAP_DEF()

};
