//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by EdgePresenter.rc
//
#define IDI_MAIN_ICON                   1
#define IDC_MAIN                        101
#define IDS_APP_TITLE                   102
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           111
#endif
#endif
