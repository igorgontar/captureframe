#pragma once
#include "MainWnd.h"
#include "EdgeFrameWnd.h"
using namespace igo;

class Monitor;

class MonitorItem
{
public:
	MonitorItem(void) : m_hCapture(0), m_hOwner(0)
	{
		memset(&m_rect, 0, sizeof(RECT));
	}
	virtual ~MonitorItem(void) 
	{   // window will be automatically destroyed in destructor of m_FrameWnd, love C++
	}

	HWND operator()(const MonitorItem& item) { return item.m_hCapture; }
	int operator()(HWND h1, HWND h2) { return h1 == h2; }

	BOOL Create(HWND hCapture)
	{
		HWND hOwner = GetParent(hCapture);
		if(!m_FrameWnd.Create(hOwner))
			return 0;

		m_hCapture = hCapture;
        m_hOwner = hOwner;

		GetWindowRect(hCapture, &m_rect);
		m_FrameWnd.SetInnerRect(m_rect);
		m_FrameWnd.Show();
		return 1;
	}

	bool AdjustRect() 
	{
		RECT rc;
		if(GetWindowRect(m_hCapture, &rc))
		{
			if(memcmp(&rc, &m_rect, sizeof(RECT)))
			{
                // this check is required to avoid frame appearing in a wrong position for a short
                // moment of time when minimizing/restoring Edge window.
                // when minimized, Edge make it's render window owned by Desktop for some reason and
                // when restored from minimized state it makes render child of the "main" window again.
                if(GetParent(m_hCapture) != m_hOwner)
                    return true;
                    
                m_rect = rc;
				m_FrameWnd.SetInnerRect(rc);
			}
			return true;
		}
		return false;  // assume the window handle is invalid
	}

	HWND GetCaptureWnd() { return m_hCapture; }

protected:
	RECT m_rect;
	HWND m_hCapture;
    HWND m_hOwner;
	EdgeFrameWnd m_FrameWnd;
};

class ItemMap : public hash_set<HWND, MonitorItem, MonitorItem, MonitorItem> {};

class Monitor : protected IRunnable
{
	enum { ATTACH = 101, DETACH, SHOW_HELP };
public:
	Monitor(MainWnd& wnd);
	virtual ~Monitor(void);
	void Start();
	void OnGuiThreadInvoke(unsigned cmd, void* param);

protected:
	MainWnd& m_invokeWnd;
	Lockable m_sync;
	Thread m_thread; 
	ItemMap m_map;
	BOOL m_helpShown;

	virtual int  Run(void* param); 
	virtual void UnblockRun() {}; 

	// bkground thread stuff
	void ScanForNew();
	void UpdatePositions();
	void Attach(HWND hCapture);
	void Detach(HWND hCapture);
	BOOL EnumTopWindowsProc(HWND hwnd);

	// GUI thread related stuff
	void BeginInvoke(unsigned cmd, void* param) { m_invokeWnd.BeginInvoke(cmd, param); }
	void OnAttach(HWND hCapture);
	void OnDetach(HWND hCapture);
	void OnShowHelp();

	static BOOL CALLBACK stEnumTopWindowsProc(HWND hwnd, LPARAM lParam);
};
