#pragma once
using namespace igo;

class TranWnd : public Wnd
{
public:
	TranWnd(void);
	virtual ~TranWnd(void);

	BOOL Create(HWND hParent, int x, int y, int dx, int dy, int opacity = 128); 
	void Show();
	void Hide();

protected:
	typedef Wnd super;
	int m_opacity;

	// message handlers
	LRESULT OnNcHitTest(WPARAM w, LPARAM l);
	LRESULT OnPaint(WPARAM w, LPARAM l);
	LRESULT OnEraseBkgnd(WPARAM w, LPARAM l);

	MSG_MAP_DEF()

};
