#include "StdAfx.h"
#include "Main.h"
#include "FrameWnd.h"

#define DX_DEFAULT 640
#define DY_DEFAULT 480
#define DY_TITLE	18 
#define DX_BORDER	 6 
#define DY_BORDER	 6 
#define DM_SIZER	32 
#define DM_BTN  	10 
#define COL_FRAME   RGB(192,16,0)
#define COL_SIZER   RGB(255,100,0)
#define COL_TEXT    RGB(255,255,255)

static char* s_szTitle = "Frame Presenter";
static char* s_szSmallTitle = "frame presenter";

//--------------------------------------------------------------------
static WndClass<FrameWnd> wnd_class(0,0,0,0);

MSG_MAP_BEG(FrameWnd, Wnd)
	ON_MSG(WM_CREATE,		OnCreate	) 
	ON_MSG(WM_DESTROY,		OnDestroy	)
	ON_MSG(WM_SIZE,			OnSize		)
	ON_MSG(WM_MOVE,			OnMove		)
	ON_MSG(WM_NCLBUTTONDOWN,OnNcLButtonDown)
	ON_MSG(WM_NCHITTEST,	OnNcHitTest	)
	ON_MSG(WM_PAINT,		OnPaint		)
	ON_MSG(WM_ERASEBKGND,	OnEraseBkgnd)
MSG_MAP_END(FrameWnd)

//--------------------------------------------------------------------
FrameWnd::FrameWnd(void) : m_opacity(0)
{
}

//--------------------------------------------------------------------
FrameWnd::~FrameWnd(void)
{
}

//--------------------------------------------------------------------
void FrameWnd::Show()
{
	ShowWindow(m_hWnd, SW_SHOW);
	m_tranWnd.Show();
}

//--------------------------------------------------------------------
void FrameWnd::Hide()
{
	m_tranWnd.Hide();
	ShowWindow(m_hWnd, SW_HIDE);
}

//--------------------------------------------------------------------
BOOL FrameWnd::Create(int opacity) 
{ 
	m_opacity = opacity;
	HWND h = super::Create(
		0
		|WS_EX_TOPMOST
		,
		0
		|WS_POPUP
		//|WS_THICKFRAME
		//|WS_CAPTION
		//|WS_SYSMENU
		,
		0,	// class name, 0: use default name
		s_szTitle,	// caption text
		CW_USEDEFAULT, CW_USEDEFAULT, DX_DEFAULT, DY_DEFAULT,
		0,	     // parent
		(UINT)0, // ID or hMenu
		0	     // hInstance 0: use default
	);
	if(!h) 
        return 0;		 	

	return 1;
}

//--------------------------------------------------------------------
LRESULT FrameWnd::OnCreate(WPARAM w, LPARAM l)
{
	SetClassLong(m_hWnd, GCL_HICON, (long)LoadIcon(GetResHandle(), MAKEINTRESOURCE(IDI_MAIN_ICON)));
	if(!m_tranWnd.Create(m_hWnd,0,0,32,32,m_opacity))
		return -1;
	return 0; //return -1; // on error
}

//--------------------------------------------------------------------
LRESULT FrameWnd::OnDestroy(WPARAM w, LPARAM l)
{
	PostQuitMessage(0);
	return 0;
}

//--------------------------------------------------------------------
void FrameWnd::SetInnerRect(const RECT& rc)
{
	RECT outer = rc;
	outer.left	 -= DX_BORDER;
	outer.right	 += DX_BORDER;
	outer.top	 -= DY_TITLE;
	outer.bottom += DY_BORDER;

	MoveWindow(m_hWnd, outer.left, outer.top, outer.right-outer.left, outer.bottom-outer.top, 1);
}

//--------------------------------------------------------------------
BOOL FrameWnd::GetInnerRect(RECT* prc)
{
	if(!prc)
		return 0;

	RECT rc = {0,0,0,0};
	if(GetWindowRect(m_hWnd, &rc))
	{
		rc.left		+= DX_BORDER;
		rc.right	-= DX_BORDER;
		rc.top		+= DY_TITLE;
		rc.bottom	-= DY_BORDER;

		*prc = rc;
	}
	return 1;
}

//--------------------------------------------------------------------
LRESULT FrameWnd::OnSize(WPARAM w, LPARAM l)
{
	RECT rc = {0,0,0,0};
	GetClientRect(m_hWnd, &rc);
	InvalidateRect(m_hWnd, &rc, false);

	int dx = rc.right - rc.left;
	int dy = rc.bottom - rc.top;
	if(dx <= 0) dx = 32;
	if(dy <= 0) dy = 16;

	RECT outer = {0,0,dx,dy};

	RECT inner = outer;
	inner.left += DX_BORDER;
	inner.top += DY_TITLE;
	inner.right -= DX_BORDER;
	inner.bottom -= DY_BORDER;

	HRGN hrgn1 = CreateRectRgnIndirect(&outer);
	HRGN hrgn2 = CreateRectRgnIndirect(&inner);
	HRGN hrgn = CreateRectRgn(0,0,1,1);

	CombineRgn(hrgn,hrgn1,hrgn2,RGN_DIFF);
	
	DeleteObject(hrgn1);
	DeleteObject(hrgn2);

	SetWindowRgn(m_hWnd, hrgn, 1);

	AdjustTranWnd();
	return 0;
}

//--------------------------------------------------------------------
LRESULT FrameWnd::OnMove(WPARAM w, LPARAM l)
{
	AdjustTranWnd();
	return 0;
}

//--------------------------------------------------------------------
void FrameWnd::AdjustTranWnd()
{
	RECT rc;
	//GetInnerRect(&rc);
	GetWindowRect(m_hWnd, &rc);
	MoveWindow(m_tranWnd.m_hWnd, rc.left, rc.top, rc.right-rc.left, rc.bottom-rc.top, 1); 
}

//--------------------------------------------------------------------
LRESULT FrameWnd::OnNcLButtonDown(WPARAM w, LPARAM l)
{
	if(w == HTMINBUTTON)
	{
		ShowWindow(m_hWnd, SW_MINIMIZE);		
		return 0;
	}
	else if(w == HTCLOSE)
	{
		DestroyWindow(m_hWnd);		
		return 0;
	}
	return super::DefWndProc(WM_NCLBUTTONDOWN, w, l);
}

//--------------------------------------------------------------------
LRESULT FrameWnd::OnNcHitTest(WPARAM wParam, LPARAM lParam)
{
	RECT rc;
	GetWindowRect(m_hWnd, &rc);
	int x = GET_X_LPARAM(lParam);
	int y = GET_Y_LPARAM(lParam);

	 // close icon
	int db = DM_BTN;
	RECT rcs = {rc.right-2-db-2, 2, rc.right-2, rc.top + 2+db+2};
	POINT pt = {x,y};
	if(PtInRect(&rcs, pt))
		return HTMINBUTTON;		

	if(    (x > rc.left && x < rc.left + DX_BORDER && y > rc.bottom - DM_SIZER)  
		|| (x > rc.left && x < rc.left + DM_SIZER && y > rc.bottom - DY_BORDER))
		return HTBOTTOMLEFT;

	if(    (x < rc.right && x > rc.right - DX_BORDER && y > rc.bottom - DM_SIZER)  
		|| (x < rc.right && x > rc.right - DM_SIZER  && y > rc.bottom - DY_BORDER))
		return HTBOTTOMRIGHT;

	return HTCAPTION;
}

//--------------------------------------------------------------------
LRESULT FrameWnd::OnEraseBkgnd(WPARAM w, LPARAM l)
{
	return 1;
}

//--------------------------------------------------------------------
LRESULT FrameWnd::OnPaint(WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	RECT rc;
	GetClientRect(m_hWnd, &rc);
	HDC hdc = BeginPaint(m_hWnd, &ps);

	FillSolidRect(hdc, &rc, COL_FRAME);
	
	SetTextColor(hdc, COL_TEXT);
	SelectObject(hdc, GetStockObject(ANSI_VAR_FONT));
	TextOut(hdc, 4, 2, s_szSmallTitle, strlen(s_szSmallTitle));

	{  // bottom right sizer
	RECT rcs = {rc.left, rc.bottom - DM_SIZER, rc.left + DM_SIZER, rc.bottom};
	FillSolidRect(hdc, &rcs, COL_SIZER);
	}

	{  // bottom left sizer
	RECT rcs = {rc.right - DM_SIZER, rc.bottom - DM_SIZER, rc.right, rc.bottom};
	FillSolidRect(hdc, &rcs, COL_SIZER);
	}

	//{  // close button
	//int db = DM_BTN;
	//RECT rcs = {rc.right-2-db-2, 2, rc.right-2, rc.top + 2+db+2};
	//FillSolidRect(hdc, &rcs, COL_SIZER);
	//	
	//	SelectObject(hdc, GetStockObject(WHITE_PEN));
	//	{
	//	POINT pt[] = {{rcs.left + 2, rcs.top+2}, {rcs.right-2, rcs.bottom-2}};
	//	Polyline(hdc, pt, sizeof(pt)/sizeof(POINT));
	//	}
	//	{
	//	POINT pt[] = {{rcs.left + 2, rcs.bottom-3}, {rcs.right-2, rcs.top+1}};
	//	Polyline(hdc, pt, sizeof(pt)/sizeof(POINT));
	//	}
	//}

	{  // minimize button
		int db = DM_BTN;
		RECT rcs = {rc.right-2-db-2, 2, rc.right-2, rc.top + 2+db+2};
		FillSolidRect(hdc, &rcs, COL_SIZER);
		
		SelectObject(hdc, GetStockObject(WHITE_PEN));
		{
		POINT pt[] = {{rcs.left + 2, rcs.bottom-3}, {rcs.right-2, rcs.bottom-3}};
		Polyline(hdc, pt, sizeof(pt)/sizeof(POINT));
		}
	}

	EndPaint(m_hWnd, &ps);
	return 0;
}



