#include "StdAfx.h"
#include "Main.h"
#include "TranWnd.h"

static char* s_szTitle = "";

//--------------------------------------------------------------------
static WndClass<TranWnd> wnd_class(0,0,0,0);

MSG_MAP_BEG(TranWnd, Wnd)
	ON_MSG(WM_NCHITTEST,	OnNcHitTest	)
	ON_MSG(WM_PAINT,		OnPaint		)
	ON_MSG(WM_ERASEBKGND,	OnEraseBkgnd)
MSG_MAP_END(TranWnd)

//--------------------------------------------------------------------
TranWnd::TranWnd(void)
{
}

//--------------------------------------------------------------------
TranWnd::~TranWnd(void)
{
}

//--------------------------------------------------------------------
void TranWnd::Show()
{
	ShowWindow(m_hWnd, SW_SHOW);
}

//--------------------------------------------------------------------
void TranWnd::Hide()
{
	ShowWindow(m_hWnd, SW_HIDE);
}

//--------------------------------------------------------------------
BOOL TranWnd::Create(HWND hParent, int x, int y, int dx, int dy, int opacity) 
{ 
	if(opacity < 0) opacity = 16;
	if(opacity > 192) opacity = 192;
	m_opacity = opacity;

	HWND h = super::Create(
		0
		|WS_EX_TOPMOST
		|WS_EX_TOOLWINDOW
		|WS_EX_LAYERED
		|WS_EX_TRANSPARENT
		,
		0
		|WS_POPUP
		,
		0,			// class name, 0: use default name
		s_szTitle,	// caption text
		x, y, dx, dy,
		hParent,			// parent
		(UINT)0,	// ID or hMenu
		0			// hInstance 0: use default
	);
	if(!h) 
        return 0;		 	

	//opacity=24;
	SetLayeredWindowAttributes(h, 0, (BYTE)opacity, LWA_ALPHA);

	return 1;
}

//--------------------------------------------------------------------
LRESULT TranWnd::OnNcHitTest(WPARAM wParam, LPARAM lParam)
{
	return HTTRANSPARENT;  // it only looks transparent for other processes if window is WS_LAYERED
}

//--------------------------------------------------------------------
LRESULT TranWnd::OnEraseBkgnd(WPARAM w, LPARAM l)
{
	HDC hdc = GetDC(m_hWnd);
	RECT rc;
	GetClientRect(m_hWnd,&rc);
	//FillSolidRect(hdc, &rc, RGB(255,255,255));
	FillSolidRect(hdc, &rc, RGB(0,0,0));
	ReleaseDC(m_hWnd, hdc);
	ValidateRect(m_hWnd, &rc);

	return 1;
}

//--------------------------------------------------------------------
LRESULT TranWnd::OnPaint(WPARAM wParam, LPARAM lParam)
{
	return 0;
}



