#pragma once
#include "TranWnd.h"
using namespace igo;

class FrameWnd : public Wnd
{
public:
	FrameWnd(void);
	virtual ~FrameWnd(void);

	BOOL Create(int opacity = 128); 
	void Show();
	void Hide();
	void SetInnerRect(const RECT& rc);
	BOOL GetInnerRect(RECT* prc);

protected:
	typedef Wnd super;

	int m_opacity;
	TranWnd m_tranWnd;

	void AdjustTranWnd();

	// message handlers
	LRESULT OnCreate(WPARAM w, LPARAM l);
	LRESULT OnDestroy(WPARAM w, LPARAM l);
	LRESULT OnSize(WPARAM w, LPARAM l);
	LRESULT OnMove(WPARAM w, LPARAM l);
	LRESULT OnNcLButtonDown(WPARAM w, LPARAM l);
	LRESULT OnNcHitTest(WPARAM w, LPARAM l);
	LRESULT OnPaint(WPARAM w, LPARAM l);
	LRESULT OnEraseBkgnd(WPARAM w, LPARAM l);

	MSG_MAP_DEF()

};
