#include "stdafx.h"
#include "Main.h"

int CALLBACK AboutProc(HWND hDlg, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
		case WM_INITDIALOG: 
			return 1;

		case WM_COMMAND:                      
			switch (LOWORD(wParam))
			{
				case IDOK:
				case IDCANCEL:
					EndDialog(hDlg, LOWORD(wParam));          
					break;
			}
			break;
	}

	return 0; 
}

int About(HWND hParent)
{
	return DialogBox(GetResHandle(), MAKEINTRESOURCE(IDD_ABOUTBOX), hParent, AboutProc);
}