#include "StdAfx.h"
#include "Main.h"
#include "PrizelWnd.h"

#define DX_DEFAULT 640
#define DY_DEFAULT 480
#define DY_TITLE	10 
#define DX_BORDER	 8 
#define DY_BORDER	 8 
#define DM_SIZER	32 
#define DM_BTN  	10 
#define COL_FRAME   RGB(192,16,0)
#define COL_SIZER   RGB(255,100,0)
#define COL_TEXT    RGB(255,255,255)

static char* s_szTitle = "Target";

//--------------------------------------------------------------------
static WndClass<PrizelWnd> wnd_class(0,0,0,0);

MSG_MAP_BEG(PrizelWnd, Wnd)
	ON_MSG(WM_CREATE,		OnCreate	) 
	ON_MSG(WM_DESTROY,		OnDestroy	)
	ON_MSG(WM_MOVE,			OnMove		)
	ON_MSG(WM_RBUTTONDOWN,  OnRButtonDown)
	ON_MSG(WM_KEYDOWN,		OnKeyDown)
	ON_MSG(WM_NCHITTEST,	OnNcHitTest	)
	ON_MSG(WM_NCLBUTTONDOWN,OnNcLButtonDown)
	ON_MSG(WM_ERASEBKGND,	OnEraseBkgnd)
	ON_MSG(WM_PAINT,		OnPaint		)
MSG_MAP_END(PrizelWnd)

//--------------------------------------------------------------------
PrizelWnd::PrizelWnd(void) : 
     m_dx(100) 
	,m_dy(100) 
	,m_bWndFromPoint(false)
	,m_hTarget(0)
{
}

//--------------------------------------------------------------------
PrizelWnd::~PrizelWnd(void)
{
}

//--------------------------------------------------------------------
void PrizelWnd::Show()
{
	ShowWindow(m_hWnd, SW_SHOW);
}

//--------------------------------------------------------------------
void PrizelWnd::Hide()
{
	ShowWindow(m_hWnd, SW_HIDE);
}

//--------------------------------------------------------------------
BOOL PrizelWnd::Create(int dx, int dy) 
{ 
	m_dx = dx;
	m_dy = dy;

	POINT pt;
	GetCursorPos(&pt);

	HWND h = super::Create(
		WS_EX_TOPMOST|WS_EX_LAYERED,
		WS_POPUP,
		0,	// class name, 0: use default name
		s_szTitle,	
		pt.x - dx/2, 
		pt.y - dy/2, 
		dx, 
		dy,
		0, // parent
		0, // ID or hMenu
		0  // hInstance 0: use default
	);
	if(!h) 
        return 0;		 	

	HRGN hrgn = CreateRoundRectRgn(0,0, dx, dy, dx, dy);
	SetWindowRgn(m_hWnd, hrgn, 1);

	BYTE opacity = 128; 
	SetLayeredWindowAttributes(m_hWnd, 0, (BYTE)opacity, LWA_ALPHA);

	m_ramkaWnd.Create(64);

	return 1;
}

//--------------------------------------------------------------------
LRESULT PrizelWnd::OnCreate(WPARAM w, LPARAM l)
{
	SetClassLong(m_hWnd, GCL_HICON, (long)LoadIcon(GetResHandle(), MAKEINTRESOURCE(IDI_MAIN_ICON)));
	return 0;
	//return -1; // on error
}

//--------------------------------------------------------------------
LRESULT PrizelWnd::OnDestroy(WPARAM w, LPARAM l)
{
	PostQuitMessage(0);
	return 0;
}

//--------------------------------------------------------------------
LRESULT PrizelWnd::OnMove(WPARAM w, LPARAM l)
{
	if(!m_ramkaWnd.m_hWnd)
		return 0;

	POINT pt;
	{
	RECT rc = {0,0,0,0};
	GetWindowRect(m_hWnd, &rc);
	
	pt.x = rc.left + (rc.right-rc.left)/2;
	pt.y = rc.top + (rc.bottom - rc.top)/2;
	}

	//ClientToScreen(hWnd, &pt);
	m_bWndFromPoint = true; // exclude this window from search
	HWND hTarget = WindowFromPoint(pt);
	m_bWndFromPoint = false;

	if(!hTarget)
		return 0;

	if(hTarget == m_ramkaWnd.m_hWnd || hTarget == m_ramkaWnd.GetTranWnd().m_hWnd)
		return 0;

	if(m_hTarget == hTarget)
		return 0;
	
	m_hTarget = hTarget;
	TraceWindowInfo(hTarget);
	//HWND h = GetDesktopWindow();

	RECT rc;
	GetWindowRect(hTarget, &rc);
	m_ramkaWnd.SetInnerRect(rc);

	if(!IsWindowVisible(m_ramkaWnd.m_hWnd))
		m_ramkaWnd.Show();

	return 0;
}

//--------------------------------------------------------------------
void PrizelWnd::TraceWindowInfo(HWND hwnd)
{
	int len = 512;
	string className(len);
	::GetClassName(hwnd, className, len);

	string caption(len);
	::GetWindowText(hwnd, caption, len);
	
	LONG dwStyle = GetWindowLong(hwnd, GWL_STYLE);
	LONG dwExStyle = GetWindowLong(hwnd, GWL_EXSTYLE);
	LONG hParent = GetWindowLongPtr(hwnd, GWL_HWNDPARENT);
	HWND hDesktop = GetDesktopWindow();

	INF_TRACE7("Tracking window HWND=0x%0X, class=%s, caption=%s, dwStyle=0x%0X, dwExStyle=0x%0X, hParent=0x%0X, desktop=0x%0X", hwnd, className, caption, dwStyle, dwExStyle, hParent, hDesktop);
}

//--------------------------------------------------------------------
LRESULT PrizelWnd::OnRButtonDown(WPARAM w, LPARAM l)
{
	DestroyWindow(m_hWnd);
	return 0;
}

//--------------------------------------------------------------------
LRESULT PrizelWnd::OnKeyDown(WPARAM w, LPARAM l)
{
	if(w == VK_ESCAPE) {
		DestroyWindow(m_hWnd);
	} else if(w == VK_RETURN) {
		DestroyWindow(m_hWnd);
	}
	return 0;
}

//--------------------------------------------------------------------
LRESULT PrizelWnd::OnNcHitTest(WPARAM wParam, LPARAM lParam)
{
	if(m_bWndFromPoint)
		return HTTRANSPARENT;
	return HTCAPTION;  
}

//--------------------------------------------------------------------
LRESULT PrizelWnd::OnNcLButtonDown(WPARAM w, LPARAM l)
{
	return super::DefWndProc(WM_NCLBUTTONDOWN, w, l);
}

//--------------------------------------------------------------------
LRESULT PrizelWnd::OnEraseBkgnd(WPARAM w, LPARAM l)
{
	HDC hdc = GetDC(m_hWnd);
	RECT rc;
	GetClientRect(m_hWnd, &rc);
	
	COLORREF bkCol = RGB(0,0,0);
	COLORREF frCol = RGB(255,255,255);

	//FillSolidRect(hdc, &rc, RGB(255,255,255));
	FillSolidRect(hdc, &rc, bkCol);
	FillSolidRect(hdc, m_dx/2, 10, 4, m_dy-10-10, frCol);
	FillSolidRect(hdc, 10, m_dy/2, m_dx-10-10, 4, frCol);
	
	ReleaseDC(m_hWnd, hdc);
	ValidateRect(m_hWnd, &rc);
	return 1;
}

//--------------------------------------------------------------------
LRESULT PrizelWnd::OnPaint(WPARAM wParam, LPARAM lParam)
{
	return 0;
}



