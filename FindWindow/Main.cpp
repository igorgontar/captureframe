#include "stdafx.h"
#include "Main.h"
#include "PrizelWnd.h"

static HINSTANCE g_hInst = 0;
HINSTANCE GetResHandle() { return g_hInst; }; 

int APIENTRY _tWinMain(HINSTANCE hInst,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	g_hInst = hInst;

	InitGdiUtils();

	PrizelWnd wnd;
	if(!wnd.Create(100,100))
		return -1;

	wnd.Show();

	MSG msg;
	HACCEL hAccelTable = LoadAccelerators(hInst, MAKEINTRESOURCE(IDC_MAIN));
	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}

