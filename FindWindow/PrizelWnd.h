#pragma once
#include "RamkaWnd.h"
using namespace igo;

class PrizelWnd : public Wnd
{
public:
	PrizelWnd(void);
	virtual ~PrizelWnd(void);

	BOOL Create(int dx, int dy); 
	void Show();
	void Hide();


protected:
	typedef Wnd super;

	int m_dx;
	int m_dy;
	bool m_bWndFromPoint;
	HWND m_hTarget;

	RamkaWnd m_ramkaWnd;

	void TraceWindowInfo(HWND hwnd);

	// message handlers
	LRESULT OnCreate(WPARAM w, LPARAM l);
	LRESULT OnDestroy(WPARAM w, LPARAM l);
	LRESULT OnMove(WPARAM w, LPARAM l);
	LRESULT OnRButtonDown(WPARAM w, LPARAM l);
	LRESULT OnKeyDown(WPARAM w, LPARAM l);
	LRESULT OnNcHitTest(WPARAM w, LPARAM l);
	LRESULT OnNcLButtonDown(WPARAM w, LPARAM l);
	LRESULT OnPaint(WPARAM w, LPARAM l);
	LRESULT OnEraseBkgnd(WPARAM w, LPARAM l);

	MSG_MAP_DEF()

};
